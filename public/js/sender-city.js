$(document).ready(
	function()
	{

		$('input[type="text"].js-city-search').autocomplete(
			{
				appendTo : 'div.js-search-location-container',
				source: window.locationAjaxUrl,
				minLength: 2
			}
		);
	}
);