<?php

    namespace App\Extension\Twig;

    use Twig\Extension\AbstractExtension;
    use Twig\TwigTest;

    /**
     * Class InstanceOfExtension
     *
     * @package App\Extension\Twig
     */
    class InstanceOfExtension extends AbstractExtension
    {

        /**
         * @return array|\Twig\TwigTest[]
         */
        public function getTests() {
            return [
                new TwigTest('instanceof', array($this, 'isInstanceOf')),
            ];
        }

        /**
         * @param $var
         * @param $instance
         *
         * @return bool
         * @throws \ReflectionException
         */
        public function isInstanceOf($var, $instance)
        {

            if($var === null)   return false;

            $reflexionClass = new \ReflectionClass($instance);
            return $reflexionClass->isInstance($var);
        }
    }