<?php

    namespace App\Entity;

    use App\Entity\Traits;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\ShopsRepository")
     * @ORM\Table(name="shop", uniqueConstraints={@ORM\UniqueConstraint(name="insales_id", columns={"insales_id"})})
     * @ORM\HasLifecycleCallbacks()
     */
    class Shop
    {

        use Traits\Id;
        use Traits\Dates;

        /**
         * @var int
         * @ORM\Column(type="integer")
         */
        protected $insalesId;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $shop;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $password;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $passwordToken;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $widgetPassword;

        /**
         * @var int
         * @ORM\Column(type="integer", nullable=true)
         */
        protected $paymentId = 0;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $paymentPassword = '';

        /**
         * @return int
         */
        public function getInsalesId() : int
        {
            return (int) $this->insalesId;
        }

        /**
         * @param int $insalesId
         * @return self
         */
        public function setInsalesId(int $insalesId) : self
        {
            $this->insalesId = $insalesId;
            return $this;
        }

        /**
         * @return string
         */
        public function getShop() : string
        {
            return $this->shop;
        }

        /**
         * @param string $shop
         * @return self
         */
        public function setShop(string $shop) : self
        {
            $this->shop = $shop;
            return $this;
        }

        /**
         * @return string
         */
        public function getPassword() : ?string
        {
            return $this->password;
        }

        /**
         * @param string $password
         * @return self
         */
        public function setPassword(string $password) : self
        {
            $this->password = $password;
            return $this;
        }

        /**
         * @return string
         */
        public function getPasswordToken() : ?string
        {
            return $this->passwordToken;
        }

        /**
         * @param string $passwordToken
         *
         * @return self
         */
        public function setPasswordToken(string $passwordToken) : self
        {
            $this->passwordToken = $passwordToken;
            return $this;
        }


        /**
         * @return string
         */
        public function getWidgetPassword() : string
        {
            return $this->widgetPassword;
        }

        /**
         * @param string $widgetPassword
         * @return self
         */
        public function setWidgetPassword(string $widgetPassword) : self
        {
            $this->widgetPassword = $widgetPassword;
            return $this;
        }

        /**
         * @return int
         */
        public function getPaymentId(): int
        {
            return $this->paymentId;
        }

        /**
         * @param int $paymentId
         * @return self
         */
        public function setPaymentId(int $paymentId): self
        {
            $this->paymentId = $paymentId;
            return $this;
        }

        /**
         * Генерация пароля для виджета
         * @return self
         */
        public function createWidgetPassword() : self
        {
            $this->setWidgetPassword(
                \md5(
                    \implode(
                        ':',
                        [
                            $this->getPassword(),
                            $this->getPasswordToken(),
                            \time()
                        ]
                    )
                )
            );

            return $this;
        }

        /**
         * @return string
         */
        public function getPaymentPassword(): string
        {
            return $this->paymentPassword;
        }

        /**
         * @param string $paymentPassword
         * @return self
         */
        public function setPaymentPassword(string $paymentPassword): self
        {
            $this->paymentPassword = $paymentPassword;
            return $this;
        }
    }