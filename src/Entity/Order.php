<?php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
     * @ORM\Table(name="orders")
     * @ORM\HasLifecycleCallbacks()
     */
    class Order
    {

        use Traits\Id;
        use Traits\Dates;
        use Traits\Shop;

        /**
         * @var integer
         * @ORM\Column(type="integer")
         */
        protected $orderId;

        /**
         * @var \DateTime
         * @ORM\Column(type="datetime")
         */
        protected $orderCreateAt;

        /**
         * @var \DateTime
         * @ORM\Column(type="datetime")
         */
        protected $orderUpdateAt;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $orderKey;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $orderNumber;

        /**
         * @var boolean
         * @ORM\Column(type="boolean")
         */
        protected $payed = false;

        /**
         * @var string
         * @ORM\Column(type="text", nullable=true)
         */
        protected $orderComment = '';

        /**
         * @var array
         * @ORM\Column(type="array", nullable=true)
         */
        protected $products = [];

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $priceOrder = 0;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $priceItems = 0;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $priceDelivery = 0;

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $deliveryTitle = '';

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $deliveryDescription = '';

        /**
         * @var int
         * @ORM\Column(type="integer")
         */
        protected $paymentId = 0;

        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $location = [];

        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $client = [];

        /**
         * @var bool
         * @ORM\Column(type="boolean")
         */
        protected $fileUploaded = false;

        /**
         * @var int
         * @ORM\Column(type="bigint", nullable=true)
         */
        protected $transactionIid = 0;

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $status = '';

        /**
         * @var array
         * @ORM\Column(type="array", nullable=true)
         */
        protected $customStatus = [];

        /**
         * @var bool
         * @ORM\Column(type="boolean")
         */
        protected $isUpdateDeal = false;

        /**
         * @return int
         */
        public function getOrderId() : int
        {
            return $this->orderId;
        }

        /**
         * @param int $orderId
         * @return self
         */
        public function setOrderId(int $orderId) : self
        {
            $this->orderId = $orderId;
            return $this;
        }

        /**
         * @return string
         */
        public function getOrderKey() : string
        {
            return $this->orderKey;
        }

        /**
         * @param string $orderKey
         * @return self
         */
        public function setOrderKey(string $orderKey) : self
        {
            $this->orderKey = $orderKey;
            return $this;
        }

        /**
         * @return string
         */
        public function getOrderNumber() : string
        {
            return $this->orderNumber;
        }

        /**
         * @param string $orderNumber
         * @return self
         */
        public function setOrderNumber(string $orderNumber) : self
        {
            $this->orderNumber = $orderNumber;
            return $this;
        }

        /**
         * @return array
         */
        public function getProducts()
        {
            return $this->products;
        }

        /**
         * @param $products
         * @return self
         */
        public function setProducts($products): self
        {
            $this->products = $products;
            return $this;
        }

        /**
         * @return string
         */
        public function getPriceOrder(): string
        {
            return $this->priceOrder;
        }

        /**
         * @param string $priceOrder
         * @return self
         */
        public function setPriceOrder(string $priceOrder): self
        {
            $this->priceOrder = $priceOrder;
            return $this;
        }

        /**
         * @return string
         */
        public function getPriceItems(): string
        {
            return $this->priceItems;
        }

        /**
         * @param string $priceItems
         * @return self
         */
        public function setPriceItems(string $priceItems): self
        {
            $this->priceItems = $priceItems;
            return $this;
        }

        /**
         * @return string
         */
        public function getPriceDelivery(): string
        {
            return $this->priceDelivery;
        }

        /**
         * @param string $priceDelivery
         * @return self
         */
        public function setPriceDelivery(string $priceDelivery): self
        {
            $this->priceDelivery = $priceDelivery;
            return $this;
        }

        /**
         * @return array
         */
        public function getLocation()
        {
            return $this->location;
        }

        /**
         * @param $location
         * @return self
         */
        public function setLocation($location): self
        {
            $this->location = $location;
            return $this;
        }

        /**
         * @return array
         */
        public function getClient()
        {
            return $this->client;
        }

        /**
         * @param $client
         * @return self
         */
        public function setClient($client): self
        {
            $this->client = $client;
            return $this;
        }

        /**
         * @return \DateTime
         */
        public function getOrderCreateAt(): \DateTime
        {
            return $this->orderCreateAt;
        }

        /**
         * @param \DateTime $orderCreateAt
         * @return self
         */
        public function setOrderCreateAt(\DateTime $orderCreateAt): self
        {
            $this->orderCreateAt = $orderCreateAt;
            return $this;
        }

        /**
         * @return \DateTime
         */
        public function getOrderUpdateAt(): \DateTime
        {
            return $this->orderUpdateAt;
        }

        /**
         * @param \DateTime $orderUpdateAt
         * @return self
         */
        public function setOrderUpdateAt(\DateTime $orderUpdateAt): self
        {
            $this->orderUpdateAt = $orderUpdateAt;
            return $this;
        }

        /**
         * @return bool
         */
        public function isPayed(): bool
        {
            return $this->payed;
        }

        /**
         * @param bool $payed
         * @return self
         */
        public function setPayed(bool $payed): self
        {
            $this->payed = $payed;
            return $this;
        }

        /**
         * @return string
         */
        public function getOrderComment(): string
        {
            return $this->orderComment ?? '';
        }

        /**
         * @param string $orderComment
         * @return self
         */
        public function setOrderComment(string $orderComment): self
        {
            $this->orderComment = $orderComment;
            return $this;
        }

        /**
         * @return int
         */
        public function getPaymentId(): int
        {
            return $this->paymentId;
        }

        /**
         * @param int $paymentId
         * @return self
         */
        public function setPaymentId(int $paymentId): self
        {
            $this->paymentId = $paymentId;
            return $this;
        }

        /**
         * @return mixed
         */
        public function getDeliveryTitle()
        {
            return $this->deliveryTitle;
        }

        /**
         * @param string $deliveryTitle
         * @return self
         */
        public function setDeliveryTitle($deliveryTitle): self
        {
            $this->deliveryTitle = $deliveryTitle;
            return $this;
        }

        /**
         * @return string
         */
        public function getDeliveryDescription()
        {
            return $this->deliveryDescription;
        }

        /**
         * @param string $deliveryDescription
         * @return self
         */
        public function setDeliveryDescription($deliveryDescription): self
        {
            $this->deliveryDescription = $deliveryDescription;
            return $this;
        }

        /**
         * Получение товаров для сделки
         * @return array
         */
        public function getItemsForDeal()
        {

            /** @var array $result */
            $result = [];

            foreach($this->getProducts() as $product)
            {

                $result[] = [
                    'sku' => !empty($product['sku']) && \mb_strlen($product['sku']) > 0
                        ? $product['sku']
                        : \implode('-', [$product['product_id'], $product['variant_id']])
                    ,
                    'name' => $product['title'],
                    'description' => $product['comment'],
                    'price' => $product['full_sale_price'],
                    'quantity' => $product['quantity'],
                ];
            }

            return $result;
        }

        /**
         * Получение дополнительных услуг (доставка)
         * @return array
         */
        public function getAdditionalServices()
        {

            /** @var array $result */
            $result = [];

            $result[] = [
                'name' => $this->getDeliveryTitle(),
                'description' => $this->getDeliveryDescription(),
                'original_cost' => $this->getPriceDelivery(),
                'final_cost' => $this->getPriceDelivery(),
                'payer' => Configuration::COMMISSION_PAYER_BUYER,
                'provided_by' => Configuration::COMMISSION_PAYER_SELLER,
            ];

            return $result;
        }

        /**
         * @return bool
         */
        public function isFileUploaded(): bool
        {
            return $this->fileUploaded;
        }

        /**
         * @param bool $fileUploaded
         * @return self
         */
        public function setFileUploaded(bool $fileUploaded): self
        {
            $this->fileUploaded = $fileUploaded;
            return $this;
        }

        /**
         * @return int
         */
        public function getTransactionIid(): int
        {
            return $this->transactionIid;
        }

        /**
         * @param int $transactionIid
         * @return self
         */
        public function setTransactionIid(int $transactionIid): self
        {
            $this->transactionIid = $transactionIid;
            return $this;
        }

        /**
         * @return string
         */
        public function getStatus(): string
        {
            return $this->status;
        }

        /**
         * @param string $status
         * @return self
         */
        public function setStatus(string $status): self
        {
            $this->status = $status;
            return $this;
        }

        /**
         * @return array
         */
        public function getCustomStatus(): array
        {
            return $this->customStatus;
        }

        /**
         * @param array $customStatus
         * @return self
         */
        public function setCustomStatus(array $customStatus): self
        {
            $this->customStatus = $customStatus;
            return $this;
        }

        /**
         * @return bool
         */
        public function isUpdateDeal(): bool
        {
            return $this->isUpdateDeal;
        }

        /**
         * @param bool $isUpdateDeal
         * @return self
         */
        public function setIsUpdateDeal(bool $isUpdateDeal): self
        {
            $this->isUpdateDeal = $isUpdateDeal;
            return $this;
        }
    }