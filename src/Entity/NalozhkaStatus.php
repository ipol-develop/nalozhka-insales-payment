<?php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\NalozhkaStatusRepository")
     * @ORM\Table(name="nalozhka_status")
     * @ORM\HasLifecycleCallbacks()
     */
    class NalozhkaStatus
    {

        use Traits\Id;
        use Traits\Dates;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $code = '';

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $title = '';

        /**
         * @return string
         */
        public function getCode() : string
        {
            return $this->code;
        }

        /**
         * @param string $code
         * @return self
         */
        public function setCode(string $code) : self
        {
            $this->code = $code;
            return $this;
        }

        /**
         * @return string
         */
        public function getTitle() : string
        {
            return $this->title;
        }

        /**
         * @param string $title
         * @return self
         */
        public function setTitle(string $title) : self
        {
            $this->title = $title;
            return $this;
        }
    }