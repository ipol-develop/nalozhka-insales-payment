<?php

    namespace App\Entity;

    use App\Entity\Traits\Order;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\DealRepository")
     * @ORM\Table(name="deal")
     * @ORM\HasLifecycleCallbacks()
     */
    class Deal
    {

        use Traits\Id;
        use Traits\Dates;
        use Traits\Shop;
        use Traits\Order;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $dealId;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $status;

        /**
         * @var \DateTime
         * @ORM\Column(type="datetime")
         */
        protected $statusUpdateAt;

        /**
         * @var string
         * @ORM\Column(type="text")
         */
        protected $conditions = '';

        /**
         * @var int
         * @ORM\Column(type="integer", nullable=true)
         */
        protected $buyerProfileId = 0;

        /**
         * @var integer
         * @ORM\Column(type="integer", nullable=true)
         */
        protected $sellerProfileId = 0;

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $confirmedBy = '';

        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $deposit = [];
        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $calculationResult = [];
        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $token = '';
        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $attachments = [];
        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $commissionPayer = Configuration::COMMISSION_PAYER_SELLER;
        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $buyerHaveToPayOffline = 0;
        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $subjectItems = [];
        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $additionalServices = [];
        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $dispute = [];
        /**
         * @var bool
         * @ORM\Column(type="boolean")
         */
        protected $partialBuyoutAllowed = false;
        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $createdByApiUser = '';
        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $subjectItemsTotal = 0;

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $marketplaceId = '';

        /**
         * @return mixed
         */
        public function getDealId()
        {
            return $this->dealId;
        }

        /**
         * @param mixed $dealId
         * @return self
         */
        public function setDealId($dealId): self
        {
            $this->dealId = $dealId;
            return $this;
        }

        /**
         * @return string
         */
        public function getStatus(): string
        {
            return $this->status;
        }

        /**
         * @param string $status
         * @return self
         */
        public function setStatus(string $status): self
        {
            $this->status = $status;
            return $this;
        }

        /**
         * @return \DateTime
         */
        public function getStatusUpdateAt(): \DateTime
        {
            return $this->statusUpdateAt;
        }

        /**
         * @param \DateTime $statusUpdateAt
         * @return self
         */
        public function setStatusUpdateAt(\DateTime $statusUpdateAt): self
        {
            $this->statusUpdateAt = $statusUpdateAt;
            return $this;
        }

        /**
         * @return string
         */
        public function getConditions(): string
        {
            return $this->conditions;
        }

        /**
         * @param string $conditions
         * @return self
         */
        public function setConditions(string $conditions): self
        {
            $this->conditions = $conditions;
            return $this;
        }

        /**
         * @return int
         */
        public function getBuyerProfileId(): int
        {
            return $this->buyerProfileId;
        }

        /**
         * @param int $buyerProfileId
         * @return self
         */
        public function setBuyerProfileId(int $buyerProfileId): self
        {
            $this->buyerProfileId = $buyerProfileId;
            return $this;
        }

        /**
         * @return int
         */
        public function getSellerProfileId(): int
        {
            return $this->sellerProfileId;
        }

        /**
         * @param int $sellerProfileId
         * @return self
         */
        public function setSellerProfileId(int $sellerProfileId): self
        {
            $this->sellerProfileId = $sellerProfileId;
            return $this;
        }

        /**
         * @return string
         */
        public function getConfirmedBy(): string
        {
            return $this->confirmedBy;
        }

        /**
         * @param string $confirmedBy
         * @return self
         */
        public function setConfirmedBy(string $confirmedBy): self
        {
            $this->confirmedBy = $confirmedBy;
            return $this;
        }

        /**
         * @return array
         */
        public function getDeposit(): array
        {
            return $this->deposit;
        }

        /**
         * @param array $deposit
         * @return self
         */
        public function setDeposit(array $deposit): self
        {
            $this->deposit = $deposit;
            return $this;
        }

        /**
         * @return array
         */
        public function getCalculationResult(): array
        {
            return $this->calculationResult;
        }

        /**
         * @param array $calculationResult
         * @return self
         */
        public function setCalculationResult(array $calculationResult): self
        {
            $this->calculationResult = $calculationResult;
            return $this;
        }

        /**
         * @return string
         */
        public function getToken(): string
        {
            return $this->token;
        }

        /**
         * @param string $token
         * @return self
         */
        public function setToken(string $token): self
        {
            $this->token = $token;
            return $this;
        }

        /**
         * @return array
         */
        public function getAttachments(): array
        {
            return $this->attachments;
        }

        /**
         * @param array $attachments
         * @return self
         */
        public function setAttachments(array $attachments): self
        {
            $this->attachments = $attachments;
            return $this;
        }

        /**
         * @return string
         */
        public function getCommissionPayer(): string
        {
            return $this->commissionPayer;
        }

        /**
         * @param string $commissionPayer
         * @return self
         */
        public function setCommissionPayer(string $commissionPayer): self
        {
            $this->commissionPayer = $commissionPayer;
            return $this;
        }

        /**
         * @return string
         */
        public function getBuyerHaveToPayOffline(): string
        {
            return $this->buyerHaveToPayOffline;
        }

        /**
         * @param string $buyerHaveToPayOffline
         * @return self
         */
        public function setBuyerHaveToPayOffline(string $buyerHaveToPayOffline): self
        {
            $this->buyerHaveToPayOffline = $buyerHaveToPayOffline;
            return $this;
        }

        /**
         * @return array
         */
        public function getSubjectItems(): array
        {
            return $this->subjectItems;
        }

        /**
         * @param array $subjectItems
         * @return self
         */
        public function setSubjectItems(array $subjectItems): self
        {
            $this->subjectItems = $subjectItems;
            return $this;
        }

        /**
         * @return array
         */
        public function getAdditionalServices(): array
        {
            return $this->additionalServices;
        }

        /**
         * @param array $additionalServices
         * @return self
         */
        public function setAdditionalServices(array $additionalServices): self
        {
            $this->additionalServices = $additionalServices;
            return $this;
        }

        /**
         * @return array
         */
        public function getDispute(): array
        {
            return $this->dispute;
        }

        /**
         * @param array $dispute
         * @return self
         */
        public function setDispute(array $dispute): self
        {
            $this->dispute = $dispute;
            return $this;
        }

        /**
         * @return bool
         */
        public function isPartialBuyoutAllowed(): bool
        {
            return $this->partialBuyoutAllowed;
        }

        /**
         * @param bool $partialBuyoutAllowed
         * @return self
         */
        public function setPartialBuyoutAllowed(bool $partialBuyoutAllowed): self
        {
            $this->partialBuyoutAllowed = $partialBuyoutAllowed;
            return $this;
        }

        /**
         * @return string
         */
        public function getCreatedByApiUser(): string
        {
            return $this->createdByApiUser;
        }

        /**
         * @param string $createdByApiUser
         * @return self
         */
        public function setCreatedByApiUser(string $createdByApiUser): self
        {
            $this->createdByApiUser = $createdByApiUser;
            return $this;
        }

        /**
         * @return string
         */
        public function getSubjectItemsTotal(): string
        {
            return $this->subjectItemsTotal;
        }

        /**
         * @param string $subjectItemsTotal
         * @return self
         */
        public function setSubjectItemsTotal(string $subjectItemsTotal): self
        {
            $this->subjectItemsTotal = $subjectItemsTotal;
            return $this;
        }

        /**
         * @return string
         */
        public function getMarketplaceId(): string
        {
            return $this->marketplaceId;
        }

        /**
         * @param string $marketplaceId
         * @return self
         */
        public function setMarketplaceId(string $marketplaceId): self
        {
            $this->marketplaceId = $marketplaceId;
            return $this;
        }

    }