<?php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\StatusRepository")
     * @ORM\Table(name="status")
     * @ORM\HasLifecycleCallbacks()
     */
    class Status
    {

        use Traits\Id;
        use Traits\Dates;
        use Traits\Shop;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $nalozhka;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $insales;

        /**
         * @return string
         */
        public function getNalozhka(): string
        {
            return $this->nalozhka;
        }

        /**
         * @param string $nalozhka
         * @return self
         */
        public function setNalozhka(string $nalozhka): self
        {
            $this->nalozhka = $nalozhka;
            return $this;
        }

        /**
         * @return string
         */
        public function getInsales(): string
        {
            return $this->insales;
        }

        /**
         * @param string $insales
         * @return self
         */
        public function setInsales(string $insales): self
        {
            $this->insales = $insales;
            return $this;
        }
    }