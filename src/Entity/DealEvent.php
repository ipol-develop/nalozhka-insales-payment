<?php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\DealEventRepository")
     * @ORM\Table(name="deal_event")
     * @ORM\HasLifecycleCallbacks()
     */
    class DealEvent
    {

        /** @var string Обновление статуса сделки */
        const HOOK_TYPE_CHANGE_STATUS = 'deals.deal.changed_status';
        /** @var string Обновление сделки */
        const HOOK_TYPE_DEAL_UPDATE = 'deals.deal.changed';
        /** @var string Создание сделки */
        const HOOK_TYPE_DEAL_CREATE = 'deals.deal.created';

        use Traits\Id;
        use Traits\Dates;
        use Traits\Shop;
        use Traits\Order;
        use Traits\Deal;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $eventId = '';

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $eventType = '';

        /**
         * @var \DateTime
         * @ORM\Column(type="datetime")
         */
        protected $occurredAt;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $oldStatus = '';

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $newStatus;

        /**
         * @var bool
         * @ORM\Column(type="boolean")
         */
        protected $exported = false;

        /**
         * @return string
         */
        public function getEventId(): string
        {
            return $this->eventId;
        }

        /**
         * @param string $eventId
         * @return self
         */
        public function setEventId(string $eventId): self
        {
            $this->eventId = $eventId;
            return $this;
        }

        /**
         * @return string
         */
        public function getEventType(): string
        {
            return $this->eventType;
        }

        /**
         * @param string $eventType
         * @return self
         */
        public function setEventType(string $eventType): self
        {
            $this->eventType = $eventType;
            return $this;
        }

        /**
         * @return \DateTime
         */
        public function getOccurredAt(): \DateTime
        {
            return $this->occurredAt;
        }

        /**
         * @param \DateTime $occurredAt
         * @return self
         */
        public function setOccurredAt(\DateTime $occurredAt): self
        {
            $this->occurredAt = $occurredAt;
            return $this;
        }

        /**
         * @return string
         */
        public function getOldStatus(): string
        {
            return $this->oldStatus;
        }

        /**
         * @param string $oldStatus
         * @return self
         */
        public function setOldStatus(string $oldStatus): self
        {
            $this->oldStatus = $oldStatus;
            return $this;
        }

        /**
         * @return string
         */
        public function getNewStatus(): string
        {
            return $this->newStatus;
        }

        /**
         * @param string $newStatus
         * @return self
         */
        public function setNewStatus(string $newStatus): self
        {
            $this->newStatus = $newStatus;
            return $this;
        }

        /**
         * @return bool
         */
        public function isExported(): bool
        {
            return $this->exported;
        }

        /**
         * @param bool $exported
         * @return self
         */
        public function setExported(bool $exported): self
        {
            $this->exported = $exported;
            return $this;
        }

    }