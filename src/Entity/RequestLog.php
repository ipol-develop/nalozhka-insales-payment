<?php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\RequestLogRepository")
     * @ORM\Table(name="request_log")
     * @ORM\HasLifecycleCallbacks()
     */
    class RequestLog
    {

        use Traits\Id;
        use Traits\Shop;
        use Traits\Dates;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $url;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $method;

        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $request = [];

        /**
         * @var array
         * @ORM\Column(type="array")
         */
        protected $response = [];

        /**
         * @return string
         */
        public function getUrl() : string
        {
            return $this->url;
        }

        /**
         * @param string $url
         * @return self
         */
        public function setUrl(string $url) : self
        {
            $this->url = $url;
            return $this;
        }

        /**
         * @return string
         */
        public function getMethod() : string
        {
            return $this->method;
        }

        /**
         * @param string $method
         * @return self
         */
        public function setMethod(string $method) : self
        {
            $this->method = $method;
            return $this;
        }

        /**
         * @return array
         */
        public function getRequest() : array
        {
            return $this->request;
        }

        /**
         * @param array $request
         * @return self
         */
        public function setRequest(array $request) : self
        {
            $this->request = $request;
            return $this;
        }

        /**
         * @return array
         */
        public function getResponse() : array
        {
            return $this->response;
        }

        /**
         * @param array $response
         * @return self
         */
        public function setResponse(array $response) : self
        {
            $this->response = $response;
            return $this;
        }

        /**
         * @return false|string
         */
        public function getResponsePrint()
        {
            return \json_encode($this->getResponse(), \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        }

        /**
         * @return false|string
         */
        public function getRequestPrint()
        {
            return \json_encode($this->getRequest(), \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        }
    }