<?php

    namespace App\Entity\Traits;

    /**
     * Trait Id
     *
     * @package App\Entity\Traits
     */
    trait Id
    {
        /**
         * @var int
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         * @ORM\Column(type="integer")
         */
        protected $id;

        /**
         * @return int
         */
        public function getId() : int
        {
            return (int) $this->id;
        }
    }