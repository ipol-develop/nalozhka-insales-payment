<?php

    namespace App\Entity\Traits;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * Trait Dates
     *
     * @package App\Entity\Traits
     * @ORM\HasLifecycleCallbacks()
     */
    trait Dates
    {

        /**
         * @var \DateTime
         * @ORM\Column(type="datetime")
         */
        protected $createAt;

        /**
         * @var \DateTime
         * @ORM\Column(type="datetime")
         */
        protected $updateAt;


        /**
         * @return \DateTime
         */
        public function getCreateAt() : \DateTime
        {
            return $this->createAt;
        }

        /**
         * @param \DateTime $createAt
         * @return self
         */
        public function setCreateAt(\DateTime $createAt) : self
        {
            $this->createAt = $createAt;
            return $this;
        }

        /**
         * @return \DateTime
         */
        public function getUpdateAt() : \DateTime
        {
            return $this->updateAt;
        }

        /**
         * @param \DateTime $updateAt
         * @return self
         */
        public function setUpdateAt(\DateTime $updateAt) : self
        {
            $this->updateAt = $updateAt;
            return $this;
        }

        /**
         * @ORM\PrePersist()
         * @throws \Exception
         */
        public function setDateCreate()
        {
            $this->setCreateAt(new \DateTime());
        }

        /**
         * @ORM\PrePersist()
         * @ORM\PreUpdate()
         * @throws \Exception
         */
        public function setDateUpdate()
        {
            $this->setUpdateAt(new \DateTime());
        }
    }