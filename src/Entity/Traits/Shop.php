<?php

    namespace App\Entity\Traits;

    /**
     * Trait Shop
     *
     * @package App\Entity\Traits
     */
    trait Shop
    {

        /**
         * @var \App\Entity\Shop
         * @ORM\ManyToOne(targetEntity="\App\Entity\Shop")
         */
        protected $shop;


        /**
         * @return \App\Entity\Shop
         */
        public function getShop() : \App\Entity\Shop
        {
            return $this->shop;
        }

        /**
         * @param \App\Entity\Shop $shop
         * @return self
         */
        public function setShop(\App\Entity\Shop $shop) : self
        {
            $this->shop = $shop;
            return $this;
        }
    }