<?php

    namespace App\Entity\Traits;

    /**
     * Trait Shop
     *
     * @package App\Entity\Traits
     */
    trait Deal
    {

        /**
         * @var \App\Entity\Deal
         * @ORM\ManyToOne(targetEntity="\App\Entity\Deal")
         */
        protected $deal;

        /**
         * @return \App\Entity\Deal
         */
        public function getDeal(): \App\Entity\Deal
        {
            return $this->deal;
        }

        /**
         * @param \App\Entity\Deal $deal
         * @return self
         */
        public function setDeal(\App\Entity\Deal $deal): self
        {
            $this->deal = $deal;
            return $this;
        }
    }