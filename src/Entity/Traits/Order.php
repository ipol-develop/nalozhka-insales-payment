<?php

    namespace App\Entity\Traits;

    /**
     * Trait Order
     *
     * @package App\Entity\Traits
     */
    trait Order
    {

        /**
         * @var \App\Entity\Order
         * @ORM\ManyToOne(targetEntity="\App\Entity\Order")
         */
        protected $order;


        /**
         * @return \App\Entity\Order
         */
        public function getOrder(): \App\Entity\Order
        {
            return $this->order;
        }

        /**
         * @param \App\Entity\Order $order
         * @return self
         */
        public function setOrder(\App\Entity\Order $order): self
        {
            $this->order = $order;
            return $this;
        }
    }