<?php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\ConfigurationRepository")
     * @ORM\Table(name="configuration")
     * @ORM\HasLifecycleCallbacks()
     */
    class Configuration
    {

        const COMMISSION_PAYER_SELLER = 'seller';
        const COMMISSION_PAYER_BUYER = 'buyer';
        const COMMISSION_PAYER_BOTH = '50/50';

        use Traits\Id;
        use Traits\Dates;
        use Traits\Shop;

        /**
         * Тестовый режим
         * @var boolean
         * @ORM\Column(type="boolean")
         */
        protected $testMode = true;

        /**
         * Боевой ключ
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $apiKey = '';

        /**
         * Тестовый ключ
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $apiKeyTest;

        /**
         * Сохранять логи
         * @var boolean
         * @ORM\Column(type="boolean")
         */
        protected $devEnv = false;

        /**
         * Обновлять статусы заказа
         * @var boolean
         * @ORM\Column(type="boolean")
         */
        protected $updateStatuses = false;

        /**
         * Использовать пользовательские статусы
         * @var boolean
         * @ORM\Column(type="boolean")
         */
        protected $useCustomStatuses = false;

        /**
         * Кто оплачивает коммисию сервиса
         * @var string
         * @ORM\Column(type="string")
         */
        protected $commissionPayer = self::COMMISSION_PAYER_SELLER;

        /**
         * @var array
         * @ORM\Column(type="array", nullable=true)
         */
        protected $statuses = [];

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $canceledStatus = '';

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        protected $canceledCustomStatus = '';

        /**
         * @return bool
         */
        public function isTestMode(): bool
        {
            return $this->testMode;
        }

        /**
         * @param bool $testMode
         * @return self
         */
        public function setTestMode(bool $testMode): self
        {
            $this->testMode = $testMode;
            return $this;
        }

        /**
         * @return string|null
         */
        public function getAuthKey()
        {
            return $this->isTestMode()
                ? $this->getApiKeyTest()
                : $this->getApiKey()
            ;
        }

        /**
         * @return string
         */
        public function getApiKey(): ?string
        {
            return $this->apiKey;
        }

        /**
         * @param string $apiKey
         * @return self
         */
        public function setApiKey(?string $apiKey = ''): self
        {
            $this->apiKey = $apiKey;
            return $this;
        }

        /**
         * @return string
         */
        public function getApiKeyTest(): ?string
        {
            return $this->apiKeyTest;
        }

        /**
         * @param string $apiKeyTest
         * @return self
         */
        public function setApiKeyTest(?string $apiKeyTest = ''): self
        {
            $this->apiKeyTest = $apiKeyTest;
            return $this;
        }

        /**
         * @return bool
         */
        public function isDevEnv(): bool
        {
            return $this->devEnv;
        }

        /**
         * @param bool $devEnv
         * @return self
         */
        public function setDevEnv(bool $devEnv): self
        {
            $this->devEnv = $devEnv;
            return $this;
        }

        /**
         * @return bool
         */
        public function isUpdateStatuses(): bool
        {
            return $this->updateStatuses;
        }

        /**
         * @param bool $updateStatuses
         * @return self
         */
        public function setUpdateStatuses(bool $updateStatuses): self
        {
            $this->updateStatuses = $updateStatuses;
            return $this;
        }

        /**
         * @return bool
         */
        public function isUseCustomStatuses(): bool
        {
            return $this->useCustomStatuses;
        }

        /**
         * @param bool $useCustomStatuses
         * @return self
         */
        public function setUseCustomStatuses(bool $useCustomStatuses): self
        {
            $this->useCustomStatuses = $useCustomStatuses;
            return $this;
        }

        /**
         * @return string
         */
        public function getCommissionPayer(): string
        {
            return $this->commissionPayer;
        }

        /**
         * @param string $commissionPayer
         * @return self
         */
        public function setCommissionPayer(string $commissionPayer): self
        {
            $this->commissionPayer = $commissionPayer;
            return $this;
        }

        /**
         * @return string
         */
        public function getCanceledStatus(): string
        {
            return $this->canceledStatus;
        }

        /**
         * @param string $canceledStatus
         * @return self
         */
        public function setCanceledStatus(string $canceledStatus): self
        {
            $this->canceledStatus = $canceledStatus;
            return $this;
        }

        /**
         * @return string
         */
        public function getCanceledCustomStatus(): string
        {
            return $this->canceledCustomStatus;
        }

        /**
         * @param string $canceledCustomStatus
         * @return self
         */
        public function setCanceledCustomStatus(string $canceledCustomStatus): self
        {
            $this->canceledCustomStatus = $canceledCustomStatus;
            return $this;
        }
    }