<?php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\TrackNumberRepository")
     * @ORM\Table(name="track_number")
     * @ORM\HasLifecycleCallbacks()
     */
    class TrackNumber
    {

        use Traits\Id;
        use Traits\Dates;
        use Traits\Order;
        use Traits\Shop;
        use Traits\Deal;

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $provider = '';

        /**
         * @var string
         * @ORM\Column(type="string")
         */
        protected $trackNumber = '';

        /**
         * @return string
         */
        public function getProvider() : string
        {
            return $this->provider;
        }

        /**
         * @param string $provider
         * @return self
         */
        public function setProvider(string $provider) : self
        {
            $this->provider = $provider;
            return $this;
        }

        /**
         * @return string
         */
        public function getTrackNumber() : string
        {
            return $this->trackNumber;
        }

        /**
         * @param string $trackNumber
         * @return self
         */
        public function setTrackNumber(string $trackNumber) : self
        {
            $this->trackNumber = $trackNumber;
            return $this;
        }
    }