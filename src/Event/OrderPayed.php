<?php

    namespace App\Event;

    use App\Entity;
    use Symfony\Component\EventDispatcher\Event;

    /**
     * Class OrderCancel
     * @package App\Event
     */
    class OrderPayed extends Event
    {

        /** @var string */
        const NAME = 'order.payed';

        /** @var Entity\Deal $deal */
        protected $deal;
        /** @var Entity\Order $order */
        protected $order;
        /** @var Entity\Shop $shop */
        protected $shop;

        /**
         * @return Entity\Deal
         */
        public function getDeal(): Entity\Deal
        {
            return $this->deal;
        }

        /**
         * @param Entity\Deal $deal
         * @return self
         */
        public function setDeal(Entity\Deal $deal): self
        {
            $this->deal = $deal;
            return $this;
        }

        /**
         * @return Entity\Order
         * @return self
         */
        public function getOrder(): Entity\Order
        {
            return $this->order;
        }

        /**
         * @param Entity\Order $order
         * @return self
         */
        public function setOrder(Entity\Order $order): self
        {
            $this->order = $order;
            return $this;
        }

        /**
         * @return Entity\Shop
         */
        public function getShop(): Entity\Shop
        {
            return $this->shop;
        }

        /**
         * @param Entity\Shop $shop
         * @return self
         */
        public function setShop(Entity\Shop $shop): self
        {
            $this->shop = $shop;
            return $this;
        }
    }