<?php

    namespace App\Event;

    use App\Entity;
    use Symfony\Component\EventDispatcher\Event;

    /**
     * Class DealChangeStatus
     * @package App\Event
     */
    class DealChangeStatus extends Event
    {

        /** @var string */
        const NAME = 'deal.status.update';

        /** @var Entity\DealEvent $dealEvent */
        protected $dealEvent;

        /**
         * @return Entity\DealEvent
         */
        public function getDealEvent(): Entity\DealEvent
        {
            return $this->dealEvent;
        }

        /**
         * @param Entity\DealEvent $dealEvent
         */
        public function setDealEvent(Entity\DealEvent $dealEvent): void
        {
            $this->dealEvent = $dealEvent;
        }
    }