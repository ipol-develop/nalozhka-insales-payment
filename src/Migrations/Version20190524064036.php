<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190524064036 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE shop (id INT AUTO_INCREMENT NOT NULL, insales_id INT NOT NULL, shop VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, password_token VARCHAR(255) NOT NULL, widget_password VARCHAR(255) NOT NULL, payment_id INT DEFAULT NULL, payment_password VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, UNIQUE INDEX insales_id (insales_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nalozhka_status (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE deals (id INT AUTO_INCREMENT NOT NULL, shop_id INT DEFAULT NULL, order_id INT DEFAULT NULL, deal_id VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, status_update_at DATETIME NOT NULL, conditions LONGTEXT NOT NULL, buyer_profile_id INT DEFAULT NULL, seller_profile_id INT DEFAULT NULL, confirmed_by VARCHAR(255) DEFAULT NULL, deposit LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', calculation_result LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', token VARCHAR(255) NOT NULL, attachments LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', commission_payer VARCHAR(255) NOT NULL, buyer_have_to_pay_offline VARCHAR(255) NOT NULL, subject_items LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', additional_services LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', dispute LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', partial_buyout_allowed TINYINT(1) NOT NULL, created_by_api_user VARCHAR(255) DEFAULT NULL, subject_items_total VARCHAR(255) NOT NULL, marketplace_id VARCHAR(255) DEFAULT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, INDEX IDX_EF39849B4D16C4DD (shop_id), INDEX IDX_EF39849B8D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE request_logs (id INT AUTO_INCREMENT NOT NULL, shop_id INT DEFAULT NULL, url VARCHAR(255) NOT NULL, method VARCHAR(255) NOT NULL, request LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', response LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, INDEX IDX_8F28E1A64D16C4DD (shop_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE deal_event (id INT AUTO_INCREMENT NOT NULL, shop_id INT DEFAULT NULL, order_id INT DEFAULT NULL, deal_id INT DEFAULT NULL, event_id VARCHAR(255) NOT NULL, event_type VARCHAR(255) NOT NULL, occurred_at DATETIME NOT NULL, old_status VARCHAR(255) NOT NULL, new_status VARCHAR(255) NOT NULL, exported TINYINT(1) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, INDEX IDX_7D71C9BB4D16C4DD (shop_id), INDEX IDX_7D71C9BB8D9F6D38 (order_id), INDEX IDX_7D71C9BBF60E2305 (deal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE track_number (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, shop_id INT DEFAULT NULL, deal_id INT DEFAULT NULL, provider VARCHAR(255) NOT NULL, track_number VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, INDEX IDX_DE52DEBD8D9F6D38 (order_id), INDEX IDX_DE52DEBD4D16C4DD (shop_id), INDEX IDX_DE52DEBDF60E2305 (deal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE statuses (id INT AUTO_INCREMENT NOT NULL, shop_id INT DEFAULT NULL, nalozhka VARCHAR(255) NOT NULL, insales VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, INDEX IDX_4BF01E114D16C4DD (shop_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders (id INT AUTO_INCREMENT NOT NULL, shop_id INT DEFAULT NULL, order_id INT NOT NULL, order_create_at DATETIME NOT NULL, order_update_at DATETIME NOT NULL, order_key VARCHAR(255) NOT NULL, order_number VARCHAR(255) NOT NULL, payed TINYINT(1) NOT NULL, order_comment LONGTEXT DEFAULT NULL, products LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', price_order VARCHAR(255) NOT NULL, price_items VARCHAR(255) NOT NULL, price_delivery VARCHAR(255) NOT NULL, delivery_title VARCHAR(255) DEFAULT NULL, delivery_description VARCHAR(255) DEFAULT NULL, payment_id INT NOT NULL, location LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', client LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', file_uploaded TINYINT(1) NOT NULL, transaction_iid BIGINT DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, custom_status LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', is_update_deal TINYINT(1) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, INDEX IDX_E52FFDEE4D16C4DD (shop_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE configuration (id INT AUTO_INCREMENT NOT NULL, shop_id INT DEFAULT NULL, test_mode TINYINT(1) NOT NULL, api_key VARCHAR(255) DEFAULT NULL, api_key_test VARCHAR(255) DEFAULT NULL, dev_env TINYINT(1) NOT NULL, update_statuses TINYINT(1) NOT NULL, use_custom_statuses TINYINT(1) NOT NULL, commission_payer VARCHAR(255) NOT NULL, statuses LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', canceled_status VARCHAR(255) DEFAULT NULL, canceled_custom_status VARCHAR(255) DEFAULT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, INDEX IDX_A5E2A5D74D16C4DD (shop_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE deals ADD CONSTRAINT FK_EF39849B4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE deals ADD CONSTRAINT FK_EF39849B8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE request_logs ADD CONSTRAINT FK_8F28E1A64D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE deal_event ADD CONSTRAINT FK_7D71C9BB4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE deal_event ADD CONSTRAINT FK_7D71C9BB8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE deal_event ADD CONSTRAINT FK_7D71C9BBF60E2305 FOREIGN KEY (deal_id) REFERENCES deals (id)');
        $this->addSql('ALTER TABLE track_number ADD CONSTRAINT FK_DE52DEBD8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE track_number ADD CONSTRAINT FK_DE52DEBD4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE track_number ADD CONSTRAINT FK_DE52DEBDF60E2305 FOREIGN KEY (deal_id) REFERENCES deals (id)');
        $this->addSql('ALTER TABLE statuses ADD CONSTRAINT FK_4BF01E114D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE configuration ADD CONSTRAINT FK_A5E2A5D74D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE deals DROP FOREIGN KEY FK_EF39849B4D16C4DD');
        $this->addSql('ALTER TABLE request_logs DROP FOREIGN KEY FK_8F28E1A64D16C4DD');
        $this->addSql('ALTER TABLE deal_event DROP FOREIGN KEY FK_7D71C9BB4D16C4DD');
        $this->addSql('ALTER TABLE track_number DROP FOREIGN KEY FK_DE52DEBD4D16C4DD');
        $this->addSql('ALTER TABLE statuses DROP FOREIGN KEY FK_4BF01E114D16C4DD');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE4D16C4DD');
        $this->addSql('ALTER TABLE configuration DROP FOREIGN KEY FK_A5E2A5D74D16C4DD');
        $this->addSql('ALTER TABLE deal_event DROP FOREIGN KEY FK_7D71C9BBF60E2305');
        $this->addSql('ALTER TABLE track_number DROP FOREIGN KEY FK_DE52DEBDF60E2305');
        $this->addSql('ALTER TABLE deals DROP FOREIGN KEY FK_EF39849B8D9F6D38');
        $this->addSql('ALTER TABLE deal_event DROP FOREIGN KEY FK_7D71C9BB8D9F6D38');
        $this->addSql('ALTER TABLE track_number DROP FOREIGN KEY FK_DE52DEBD8D9F6D38');
        $this->addSql('DROP TABLE shop');
        $this->addSql('DROP TABLE nalozhka_status');
        $this->addSql('DROP TABLE deals');
        $this->addSql('DROP TABLE request_logs');
        $this->addSql('DROP TABLE deal_event');
        $this->addSql('DROP TABLE track_number');
        $this->addSql('DROP TABLE statuses');
        $this->addSql('DROP TABLE orders');
        $this->addSql('DROP TABLE configuration');
    }
}
