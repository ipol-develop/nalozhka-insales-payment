<?php

    namespace App\Services\Nalozhka;

    use App\Entity;
    use Doctrine\ORM\EntityManagerInterface;
    use Psr\Log\LoggerInterface;
    use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Routing\RouterInterface;

    /**
     * Class Client
     * @package App\Services\Nalozhka
     */
    class Client
    {

        /** @var string Боевой url */
        const API_URL = 'https://deals.api.nalogka.ru/';
        /** @var string Тестовый url */
        const API_URL_TEST = 'https://sandbox.deals.api.nalogka.ru/';

        /** @var string Страница оплаты */
        const PAYMENT_URL = 'https://cabinet.nalogka.ru/deals/{deal_id}/payment';
        /** @var string Страница оплаты - тест */
        const PAYMENT_URL_TEST = 'https://sandbox.cabinet.nalogka.ru/deals/{deal_id}/payment';

        /** @var string Сервер загрузки файлов */
        const API_FILE_URL = 'https://filestorage.api.nalogka.ru/';
        /** @var string Сервер загрузки файлов */
        const API_FILE_URL_TEST = 'https://sandbox.filestorage.api.nalogka.ru/';

        /** @var Entity\Shop $shop */
        protected $shop;
        /** @var Entity\Configuration $configuration */
        protected $configuration;
        /** @var EntityManagerInterface $entityManager */
        protected $entityManager;
        /** @var RouterInterface $router */
        protected $router;
        /** @var ParameterBagInterface $parameterBag */
        protected $parameterBag;
        /** @var bool $noLog */
        public static $noLog = false;
        /** @var \Psr\Log\LoggerInterface $logger */
        protected $logger;
        /** @var null|ApiResponse */
        protected $lastResponse = null;

        /**
         * Client constructor.
         *
         * @param EntityManagerInterface $entityManager
         * @param RouterInterface $router
         * @param ParameterBagInterface $parameterBag
         * @param \Psr\Log\LoggerInterface $logger
         */
        public function __construct(
            EntityManagerInterface $entityManager,
            RouterInterface $router,
            ParameterBagInterface $parameterBag,
            LoggerInterface $logger
        )
        {
            $this->entityManager = $entityManager;
            $this->router = $router;
            $this->parameterBag = $parameterBag;
            $this->logger = $logger;
        }

        /**
         * @param Entity\Shop $shop
         * @return self
         */
        public function setShop(Entity\Shop $shop): self
        {
            $this->shop = $shop;
            return $this;
        }

        /**
         * @param Entity\Configuration $configuration
         * @return self
         */
        public function setConfiguration(Entity\Configuration $configuration): self
        {
            $this->configuration = $configuration;
            return $this;
        }

        /**
         * @param $url
         * @param $server
         * @return string
         */
        protected function getUrl($url, $server)
        {

            if($server !== null)
                return $server . $url;

            return $this->configuration->isTestMode()
                ? self::API_URL_TEST . $url
                : self::API_URL . $url
            ;
        }

        /**
         * @param $url
         * @param $method
         * @param array $params
         * @param null $headers
         * @param null $server
         * @return ApiResponse
         */
        protected function sendRequest($url, $method, $params = [], $headers = null, $server = null)
        {

            /** @var array $responseHeaders */
            $responseHeaders = [];

            $ch = \curl_init();

            \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            \curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            \curl_setopt($ch, CURLOPT_FAILONERROR, false);
            \curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            \curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            \curl_setopt($ch, CURLOPT_TIMEOUT, 30);

            if (empty($headers))
            {
                $headers[] = 'Content-Type: application/json';
            }

            $headers[] = 'Cache-Control: no-cache';
            $headers[] = 'X-Nalogka-Auth-Token: ' . $this->configuration->getAuthKey();

            \curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            /** @var string $url */
            $url = $this->getUrl($url, $server);

            if (Request::METHOD_GET === $method && !empty($params))
                $url .= '?' . \http_build_query($params);

            if (Request::METHOD_POST === $method)
            {

                \curl_setopt($ch, CURLOPT_POST, true);
                \curl_setopt($ch, CURLOPT_POSTFIELDS, null);

                if(!empty($params) && (\count($params) > 0 || \mb_strlen($params) > 0))
                {
                    \curl_setopt(
                        $ch,
                        CURLOPT_POSTFIELDS,
                        $server === null
                            ? \json_encode($params)
                            : $params
                    );
                }

                if(!empty($params) && !empty($params['_file_upload']) && (bool) $params['_file_upload'])
                {
                    $point = \fopen($params['file'], 'r');

                    \curl_setopt($ch, CURLOPT_INFILE, $point);
                    \curl_setopt($ch, CURLOPT_INFILESIZE, $params['fileSize']);
                }
            }

            if (
                \in_array(
                    $method,
                    [
                        Request::METHOD_PATCH,
                        Request::METHOD_PUT
                    ]
                )
            )
            {

                \curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
                \curl_setopt(
                    $ch,
                    CURLOPT_POSTFIELDS,
                    $server === null
                        ? \json_encode($params)
                        : $params
                );
            }

            if (Request::METHOD_DELETE === $method)
                \curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

            \curl_setopt(
                $ch,
                CURLOPT_HEADERFUNCTION,
                function($ch, $header) use (&$responseHeaders)
                {

                    unset($ch);

                    if (\substr_count($header, ':'))
                    {
                        list($key, $value) = \explode(':', $header, 2);
                        $responseHeaders[$key] = \trim($value);
                    }
                    else
                        $responseHeaders[] = \trim($header);

                    $responseHeaders = \array_filter($responseHeaders);

                    return \strlen($header);
                }
            );

            \curl_setopt($ch, CURLOPT_URL, $url);

            $response = \json_decode(\curl_exec($ch), true) ?? [];
            $info = \curl_getinfo($ch);

            \curl_close($ch);

            if(!empty($point) && \is_resource($point))
                \fclose($point);

            if($this->configuration->isDevEnv() && !self::$noLog)
            {
                try
                {
                    $this->entityManager
                        ->getRepository(Entity\RequestLog::class)
                        ->saveLog(
                            $this->shop,
                            $url,
                            $method,
                            \is_array($params) ? $params : [$params],
                            $response
                        )
                    ;
                }
                catch(\Exception $exception)
                {
                    $this->logger->critical(
                        'error log request',
                        [
                            'message' => $exception->getMessage(),
                            'file' => $exception->getFile(),
                            'line' => $exception->getLine(),
                            'shop' => $this->shop->getShop(),
                            'url' => $url,
                            'method' => $method,
                            'params' => $params,
                            'response' => $response
                        ]
                    );
                }
            }

            return $this->lastResponse = new ApiResponse(
                $info['http_code'],
                $response,
                $responseHeaders
            );
        }

        /**
         * Получение возможных статусов сделки
         * @return array
         */
        public function getStatuses()
        {

            /** @var ApiResponse $response */
            $response = $this->sendRequest(
                'deals/statuses',
                Request::METHOD_GET
            );

            if(
                !$response->isSuccessful()
                || !\is_array($response->getData()['collection'])
            )
                return [];

            /** @var array $result */
            $result = [];

            foreach ($response->getData()['collection'] as $status)
            {
                $result[] = [
                    'type' => $status['~type'],
                    'id' => $status['~id'],
                    'title' => $status['name'],
                ];
            }

            return $result;
        }

        /**
         * Создание сделки
         * @param Entity\Shop $shop
         * @param Entity\Order $order
         * @param Entity\Configuration $configuration
         * @return Entity\Deal|null
         * @throws \Exception
         */
        public function createDeal(
            Entity\Shop $shop,
            Entity\Order $order,
            Entity\Configuration $configuration
        ): ?Entity\Deal
        {

            $deal = $this->entityManager
                ->getRepository(Entity\Deal::class)
                ->getByShopOrder($order);

            if($deal instanceof Entity\Deal)
                return $deal;

            /** @var array $fields */
            $fields = [
                'order_id' => $order->getOrderNumber(),
                'conditions' => '',
                'commission_payer' => $configuration->getCommissionPayer(),
                'buyer_have_to_pay_offline' => 0,
                'subject_items' => $order->getItemsForDeal(),
                'additional_services' => $order->getAdditionalServices(),
                'partial_buyout_allowed' => true,
            ];

            /** @var ApiResponse $response */
            $response = $this->sendRequest(
                'deals/',
                Request::METHOD_POST,
                $fields
            );

            if(!$response->isSuccessful())
                return null;

            /** @var array $fields */
            $fields = $response->getData();

            /** @var Entity\Deal $deal */
            $deal = new Entity\Deal();

            $deal->setShop($shop)
                ->setOrder($order)
                ->setDealId($fields['~id'])
                ->setMarketplaceId($fields['marketplace_id'])
                ->setStatus($fields['status'])
                ->setStatusUpdateAt(new \DateTime($fields['status_updated_at']))
                ->setConditions($fields['conditions'])
                ->setBuyerProfileId($fields['buyer_profile_id'] ?? 0)
                ->setSellerProfileId($fields['seller_profile_id'] ?? 0)
                ->setConfirmedBy($fields['confirmed_by'] ?? '')
                ->setDeposit(
                !empty($fields['deposit'])
                    && \is_array($fields['deposit'])
                        ? $fields['deposit']
                        : []
                )

                ->setCalculationResult(
                !empty($fields['calculation_result'])
                    && \is_array($fields['calculation_result'])
                        ? $fields['calculation_result']
                        : []
                )
                ->setToken($fields['token'])
                ->setAttachments($fields['attachments'] ?? [])
                ->setCommissionPayer($fields['commission_payer'])
                ->setBuyerHaveToPayOffline($fields['buyer_have_to_pay_offline'])
                ->setSubjectItems($fields['subject_items'])
                ->setAdditionalServices(
                    !empty($fields['additional_services'])
                    && \is_array($fields['additional_services'])
                        ? $fields['additional_services']
                        : []
                )->setDispute(
                    !empty($fields['dispute'])
                    && \is_array($fields['dispute'])
                        ? $fields['dispute']
                        : []
                )
                ->setPartialBuyoutAllowed($fields['partial_buyout_allowed'] === 1)
                ->setCreatedByApiUser($fields['created_by_api_user'] ?? '')
                ->setSubjectItemsTotal($fields['subject_items_total'] ?? '')
            ;

            $this->entityManager->persist($deal);
            $this->entityManager->flush();

            return $deal;
        }

        /**
         * @param Entity\Deal $deal
         * @param Entity\Order $order
         * @return array
         */
        public function getPaymentUrl(Entity\Deal $deal, Entity\Order $order)
        {
            return [
                'url' => \strtr(
                    $this->configuration->isTestMode()
                        ? self::PAYMENT_URL_TEST
                        : self::PAYMENT_URL,
                    [
                        '{deal_id}' => $deal->getDealId()
                    ]
                ),
                'fields' => [
                    'buyer' => $order->getClient()['name'],
                    'buyer_email' => $order->getClient()['email'],
                    'buyer_phone' => $order->getClient()['phone'],
                    'success_url' => $this->parameterBag->get('app.http')
                        . $this->router->generate(
                            'payment.redirect.url',
                            [
                                'type' => 'success',
                                'dealId' => $deal->getDealId()
                            ]
                        ),
                    'fail_url' => $this->parameterBag->get('app.http')
                        . $this->router->generate(
                            'payment.redirect.url',
                            [
                                'type' => 'fail',
                                'dealId' => $deal->getDealId()
                            ]
                        ),
                ]
            ];
        }

        /**
         * Загрузка файла на сервер
         * @param array $fields
         * @return ApiResponse
         */
        public function uploadFile(array $fields)
        {
            return $this->sendRequest(
                'upload?filename=' . $fields['filename'],
                Request::METHOD_POST,
                [
                    '_file_upload' => true,
                    'file' => $fields['file'],
                    'fileSize' => $fields['fileSize'],
                ],
                $fields['header'],
                $this->configuration->isTestMode()
                    ? self::API_FILE_URL_TEST
                    : self::API_FILE_URL
            );
        }

        /**
         * Привязка фотографий к сделке
         *
         * @param \App\Entity\Deal $deal
         * @param array $fields
         *
         * @return \App\Services\Nalozhka\ApiResponse
         */
        public function appendFileToDeal(Entity\Deal $deal, array $fields)
        {
            return $this->sendRequest(
                '/deals/' . $deal->getDealId() . '/add-attachments',
                Request::METHOD_POST,
                [
                    'attachments' => $fields,
                ]
            );
        }

        /**
         * Отмена сделки
         * @param Entity\Deal $deal
         * @return ApiResponse
         */
        public function cancelDeal(Entity\Deal $deal)
        {
            return $this->sendRequest(
                'deals/'.$deal->getDealId().'/cancel',
                Request::METHOD_POST
            );
        }

        /**
         * Получение провайдеров трек-номеров
         * @return array
         */
        public function getTrackProviders()
        {
            return [
                'cdek' => [
                    'title' => 'СДЭК',
                    'provider' => 'cdek'
                ]
            ];
        }

        /**
         * Создание трек-номера
         *
         * @param \App\Entity\Deal $deal
         * @param \App\Entity\TrackNumber $trackNumber
         *
         * @return \App\Services\Nalozhka\ApiResponse
         */
        public function createTrackNumber(Entity\Deal $deal, Entity\TrackNumber $trackNumber)
        {
            return $this->sendRequest(
                'deals/' . $deal->getDealId() . '/performing-track',
                Request::METHOD_PUT,
                [
                    'tracker_short_name' => $trackNumber->getProvider(),
                    'code' => $trackNumber->getTrackNumber(),
                ]
            );
        }

        /**
         * Удаление трек-номера
         *
         * @param \App\Entity\Deal $deal
         * @param \App\Entity\TrackNumber $trackNumber
         *
         * @return \App\Services\Nalozhka\ApiResponse
         */
        public function removeTrackNumber(Entity\Deal $deal, Entity\TrackNumber $trackNumber)
        {
            return $this->sendRequest(
                'deals/'.$deal->getDealId().'/performing-track',
                Request::METHOD_DELETE,
                [
                    'tracker_short_name' => $trackNumber->getProvider(),
                    'code' => $trackNumber->getTrackNumber(),
                ]
            );
        }

        /**
         * Перевод сделки в исполнение
         *
         * @param \App\Entity\Deal $deal
         *
         * @return \App\Services\Nalozhka\ApiResponse
         */
        public function performingStart(Entity\Deal $deal)
        {
            return $this->sendRequest(
                    '/deals/'.$deal->getDealId().'/performing-start',
                Request::METHOD_POST
            );
        }

        /**
         * Получение временного ключа
         *
         * @param array $fields
         *
         * @return \App\Services\Nalozhka\ApiResponse
         */
        public function getTemporaryAccessToken(array $fields)
        {
            return $this->sendRequest(
                'temporary-access-token',
                Request::METHOD_POST,
                \json_encode($fields),
                null,
                $this->configuration->isTestMode()
                    ? self::API_FILE_URL_TEST
                    : self::API_FILE_URL
            );
        }

        /**
         * @return ApiResponse|null
         */
        public function getLastResponse(): ?ApiResponse
        {
            return $this->lastResponse;
        }
    }