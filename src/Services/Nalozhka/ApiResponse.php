<?php

    namespace App\Services\Nalozhka;

    /**
     * Class ApiResponse
     * @package App\Services\Nalozhka
     */
    class ApiResponse
    {

        /** @var  int $httpCode Код ответа */
        protected $httpCode;
        /** @var array $headers Заголовки */
        protected $headers;

        /** @var array $response */
        private $response;

        /**
         * ApiResponse constructor.
         * @param $httpCode
         * @param array $data
         * @param array $headers
         */
        public function __construct($httpCode, $data = [], $headers = [])
        {
            $this->httpCode = $httpCode;
            $this->headers = $headers;
            $this->response = $data;
        }

        /**
         * @return bool
         */
        public function isSuccessful()
        {
            return in_array(
                $this->httpCode,
                [201, 200, 204]
            );
        }

        /**
         * @return int
         */
        public function getHttpCode()
        {
            return $this->httpCode;
        }

        /**
         * @return array
         */
        public function getHeaders()
        {
            return $this->headers;
        }

        /**
         * @param array $headers
         */
        public function setHeaders($headers)
        {
            $this->headers = $headers;
        }

        public function getData()
        {
            return $this->response;
        }

        public function setData($data)
        {
            $this->response = $data;
        }

        /**
         * @param int $httpCode
         */
        public function setHttpCode($httpCode)
        {
            $this->httpCode = $httpCode;
        }
    }