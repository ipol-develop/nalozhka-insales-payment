<?php

    namespace App\Services;

    use Doctrine\ORM\EntityManagerInterface;
    use Knp\Component\Pager\PaginatorInterface;
    use App\Entity;

    /**
     * Class Logs
     * @package App\Services
     */
    class Logs
    {

        /** @var EntityManagerInterface $entityManager */
        protected $entityManager;
        /** @var PaginatorInterface $paginator */
        protected $paginator;

        /**
         * Logs constructor.
         * @param EntityManagerInterface $entityManager
         * @param PaginatorInterface $paginator
         */
        public function __construct(EntityManagerInterface $entityManager, PaginatorInterface $paginator)
        {
            $this->entityManager = $entityManager;
            $this->paginator = $paginator;
        }


        /**
         * @param Entity\Shop $shop
         * @param int $page
         * @param int $limit
         *
         * @return Entity\RequestLog[]|\Knp\Component\Pager\Pagination\PaginationInterface
         */
        public function getLogsByShop(Entity\Shop $shop, $page = 1, $limit = 20)
        {

            $query = $this->entityManager
                ->getRepository(Entity\RequestLog::class)
                ->getLogs($shop);

            return $this->paginator->paginate($query, $page, $limit);
        }
    }