<?php

    namespace App\Services\Insales;

    use App\Entity;
    use InSales\API\ApiResponse;
    use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

    /**
     * Class Client
     * @package App\Services\Insales
     */
    class Client
    {

        /** @var Entity\Shop $shop */
        protected $shop;
        /** @var Entity\Configuration $configuration */
        protected $configuration;
        /** @var string $appId */
        protected $appId = '';
        /** @var \InSales\API\ApiClient $client */
        protected $client;
        /** @var \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface $parameterBag */
        protected $parameterBag;

        /**
         * Client constructor.
         *
         * @param \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface $parameterBag
         */
        public function __construct(ParameterBagInterface $parameterBag)
        {
            $this->parameterBag = $parameterBag;
            $this->appId = $this->parameterBag->get('app.id');
        }

        /**
         * @param Entity\Shop $shop
         * @return self
         */
        public function setShop(Entity\Shop $shop): self
        {

            $this->shop = $shop;

            $this->client = new \InSales\API\ApiClient(
                $this->appId,
                $shop->getPassword(),
                $shop->getShop()
            );

            return $this;
        }

        /**
         * @param Entity\Configuration $configuration
         * @return self
         */
        public function setConfiguration(Entity\Configuration $configuration): self
        {
            $this->configuration = $configuration;
            return $this;
        }

        /**
         * Получение статусов заказа insales
         * @return array
         */
        public function getStatuses()
        {

            /** @var array $result */
            $result = [];

            foreach($this->client->getCustomStatuses()->getData() as $status)
                $result[] = $status;

            return $result;
        }

        /**
         * Получение статусов заказа insales для связи с отменой заказ
         * @return array
         */
        public function getStatusesCancel()
        {

            /** @var array $result */
            $result = [];

            foreach($this->getStatuses() as $status)
                $result[$status['title']] = $status['permalink'];

            return $result;
        }

        /**
         * Получение фотографий товара
         * @param int $productId
         * @return array
         */
        public function getProductImage(int $productId)
        {

            /** @var ApiResponse $response */
            $response = $this->client->getProductById($productId);

            if(!$response->isSuccessful())
                return [];

            return $response->getData()['images'] ?? [];
        }

        /**
         * Обновление статуса заказа
         * @param $orderId
         * @param $orderStatus
         * @return ApiResponse
         */
        public function setOrderStatus($orderId, $orderStatus)
        {
            return $this->client->updateOrder(
                $orderId,
                [
                    'fulfillment_status' => $orderStatus,
                ]
            );
        }
        /**
         * Обновление пользовательского статуса заказа
         * @param $orderId
         * @param $orderStatus
         * @return ApiResponse
         */
        public function setOrderCustomStatus($orderId, $orderStatus)
        {
            return $this->client->updateOrder(
                $orderId,
                [
                    'custom_status_permalink' => $orderStatus,
                ]
            );
        }

        /**
         * Оплата заказа
         * @param \App\Entity\Order $order
         * @param bool $payed
         *
         * @return \InSales\API\ApiResponse
         */
        public function setOrderPayed(Entity\Order $order, $payed = true)
        {
            return $this->client->updateOrder(
                $order->getOrderId(),
                [
                    'financial_status' => $payed
                        ? $this->parameterBag->get('app.insales.payed.payed')
                        : $this->parameterBag->get('app.insales.payed.no-payed')
                ]
            );
        }
    }