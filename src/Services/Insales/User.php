<?php

    namespace App\Services\Insales;

    use App\Entity;

    use Symfony\Component\HttpFoundation\RequestStack;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\HttpFoundation\RedirectResponse;
    use Symfony\Component\Routing\RouterInterface;

    /**
     * Class User
     *
     * @package App\Services\Insales
     */
    class User
    {

        /** @var \Symfony\Component\HttpFoundation\Request */
        private $request;
        /** @var RouterInterface */
        private $router;
        /** @var EntityManagerInterface */
        private $entityManager;
        /** @var Entity\Shop */
        private $shop;
        /** @var bool $isAuthorized */
        private $isAuthorized = false;
        /** @var Session  */
        private $session;
        private $return;
        /** @var string $appId */
        protected $appId;
        /** @var string $appSecret */
        protected $appSecret;
        /** @var string $appHost */
        protected $appHost;

        /**
         * User constructor.
         *
         * @param $appId
         * @param $appSecret
         * @param $appHost
         * @param \Doctrine\ORM\EntityManagerInterface $entityManager
         * @param \Symfony\Component\Routing\RouterInterface $router
         * @param \Symfony\Component\HttpFoundation\RequestStack $request
         */
        public function __construct(
            $appId,
            $appSecret,
            $appHost,
            EntityManagerInterface $entityManager,
            RouterInterface $router,
            RequestStack $request
        )
        {

            $this->appId = $appId;
            $this->appSecret = $appSecret;
            $this->appHost = $appHost;
            $this->router = $router;
            $this->entityManager = $entityManager;

            $this->request = $request->getCurrentRequest();
            $this->session = $this->request->getSession();
        }

        /**
         * @return bool
         */
        public function isAuthorized(): bool
        {
            return $this->isAuthorized;
        }

        public function getAuthorize()
        {

            if(
                (int) \mb_strlen($this->session->get('insalesRequestToken')) > 0
                && (int) \mb_strlen($this->session->get('insalesUserToken')) > 0
                && (int) $this->session->get('insalesId')	> 0
            )
            {

                /** @var Entity\Shop */
                $this->shop = $this->entityManager->getRepository(Entity\Shop::class)->findOneBy(
                    [
                        'insalesId' => $this->session->get('insalesId'),
                        'passwordToken' => $this->session->get('insalesUserToken'),
                    ]
                );

                if($this->shop instanceof Entity\Shop)
                {

                    $this->isAuthorized = true;
                    return true;
                }
            }

            if(
                (int) $this->request->query->get('insales_id') > 0
                && (int) \mb_strlen($this->request->query->get('token2')) === 0
            )
            {

                $this->session->remove('insalesId');
                $this->session->remove('insalesRequestToken');
                $this->session->remove('insalesUserToken');

                /** @var Entity\Shop */
                $this->shop = $this->entityManager->getRepository(Entity\Shop::class)
                    ->getByInsalesId($this->request->query->getInt('insales_id'));

                $this->shop->setPasswordToken($this->createToken());

                $this->entityManager->persist($this->shop);
                $this->entityManager->flush();

                $this->session->set('insalesId', $this->shop->getInsalesId());
                $this->session->set('insalesRequestToken', $this->shop->getPasswordToken());

                $this->return = new RedirectResponse(
                    'http://'.$this->shop->getShop()
                    . '/admin/applications/'
                    . $this->appId
                    . '/login?token='
                    . $this->shop->getPasswordToken()
                    . '&login='
                    . $this->appHost
                    . $this->router->generate('app.auth')
                );
            }

            if(
                \mb_strlen($this->request->query->get('token')) > 0
                && \mb_strlen($this->request->query->get('token2')) > 0
                && \intval($this->request->getSession()->get('insalesId')) > 0
            )
            {

                /** @var Entity\Shop */
                $this->shop = $this->entityManager
                    ->getRepository(Entity\Shop::class)
                    ->getByInsalesId((int) $this->session->get('insalesId'));

                if($this->checkToken())
                {

                    $this->session->set('insalesUserToken', $this->request->query->get('token2'));
                    $this->shop->setPasswordToken($this->request->query->get('token2'));

                    $this->entityManager->persist($this->shop);
                    $this->entityManager->flush();

                    $this->return = new RedirectResponse(
                        $this->router->generate('app.configure')
                    );
                }
            }

            return false;
        }

        /**
         * Проверка валидности токена
         *
         * @return bool
         */
        private function checkToken()
        {

            return
                $this->request->query->get('token2') === \md5(
                    $this->shop->getPasswordToken()
                    . $this->request->query->get('user_email')
                    . $this->request->query->get('user_name')
                    . $this->request->query->get('user_id')
                    . $this->shop->getPassword()
                )
                ;
        }

        /**
         * @return string
         */
        private function createToken()
        {
            return \md5(\microtime().\md5(\microtime()));
        }

        /**
         * @return mixed
         */
        public function getReturn()
        {
            return $this->return;
        }

        /**
         * Проверка авторизации приложения
         *
         * @return bool
         */
        public function checkAuthorize()
        {

            if((int) $this->session->get('insalesId') === 0)
                return false;

            if((int) \mb_strlen($this->session->get('insalesUserToken')) === 0)
                return false;

            /** @var Entity\Shop $shop */
            $shop = $this->entityManager->getRepository(Entity\Shop::class)
                ->getByInsalesId((int) $this->session->get('insalesId'));

            if(!$shop instanceof Entity\Shop)
                return false;

            $this->isAuthorized = true;

            return true;
        }

        /**
         * Получение insalesId магазина
         *
         * @return int
         */
        public function getInsalesId()
        {
            return (int) $this->session->get('insalesId');
        }
    }