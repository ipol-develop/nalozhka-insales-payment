<?php

    namespace App\Services\Insales;

    use App\Entity\Shop;
    use Doctrine\ORM\EntityManagerInterface;
    use function md5;
    use Psr\Log\LoggerInterface;
    use Symfony\Component\Routing\RouterInterface;
    use App\Entity;
    use \InSales\API;
    use Symfony\Contracts\Translation\TranslatorInterface;

    /**
     * Class Installer
     *
     * @package App\Services\Insales
     */
    class Installer
    {

        /** @var string $appId */
        protected $appId;
        /** @var string $appSecret */
        protected $appSecret;
        /** @var string $appUrl */
        protected $appUrl;
        /** @var EntityManagerInterface $entityManager */
        protected $entityManager;
        /** @var RouterInterface $router */
        protected $router;
        /** @var Shop $shop */
        protected $shop;
        /** @var TranslatorInterface $translator */
        protected $translator;
        /** @var LoggerInterface $logger */
        protected $logger;

        /**
         * Installer constructor.
         *
         * @param string $appId
         * @param string $appSecret
         * @param string $appUrl
         * @param EntityManagerInterface $entityManager
         * @param RouterInterface $router
         * @param TranslatorInterface $translator
         * @param LoggerInterface $logger
         */
        public function __construct(
            string $appId,
            string $appSecret,
            string $appUrl,
            EntityManagerInterface $entityManager,
            RouterInterface $router,
            TranslatorInterface $translator,
            LoggerInterface $logger
        )
        {

            $this->appId = $appId;
            $this->appSecret = $appSecret;
            $this->appUrl = $appUrl;
            $this->router = $router;
            $this->entityManager = $entityManager;
            $this->translator = $translator;
            $this->logger = $logger;
        }

        /**
         * Получение пароля из токена
         * @param string $token
         * @return string
         */
        public function getPasswordFromToken($token)
        {
            return md5($token . $this->appSecret);
        }

        /**
         * Установка приложения
         *
         * @param Shop $shop
         *
         * @return bool
         */
        public function installApplication(Shop $shop)
        {

            /** @var API\ApiClient $client */
            $client = new API\ApiClient(
                $this->appId,
                $shop->getPassword(),
                $shop->getShop()
            );

            /** @var Api\ApiResponse $response */
            $response = $client->createWebhook(
                [
                    'address' => $this->appUrl . $this->router->generate('webhook.order'),
                    'topic' => 'orders/update',
                    'format_type' => 'json',
                ]
            );

            if(!$response->isSuccessful())
            {

                $this->logger->error(
                    'error set orders/update webhook',
                    [
                        'shop' => $shop->getShop(),
                        'response' => [
                            'data' => $response->getData(),
                            'header' => $response->getHeaders(),
                            'message' => $response->getMessage()
                        ]
                    ]
                );

                return false;
            }

            /** @var Api\ApiResponse $response */
            $response = $client->createWebhook(
                [
                    'address' => $this->appUrl . $this->router->generate('webhook.order'),
                    'topic' => 'orders/create',
                    'format_type' => 'json',
                ]
            );

            if(!$response->isSuccessful())
            {

                $this->logger->error(
                    'error set orders/create webhook',
                    [
                        'shop' => $shop->getShop(),
                        'response' => [
                            'data' => $response->getData(),
                            'header' => $response->getHeaders(),
                            'message' => $response->getMessage()
                        ]
                    ]
                );

                return false;
            }

            /** @var Api\ApiResponse $response */
            $response = $client->createPaymentGateway(
                [
                    'title' => $this->translator->trans('installer.payment.name'),
                    'type' => 'PaymentGateway::External',
                    'url' => $this->appUrl . $this->router->generate('payment.url.pay'),
                    'shop_id' => $shop->getInsalesId(),
                    'margin' => '0',
                    'position' => '100',
                ]
            );

            if(!$response->isSuccessful())
            {

                $this->logger->error(
                    'error create pay system',
                    [
                        'shop' => $shop->getShop(),
                        'response' => [
                            'data' => $response->getData(),
                            'header' => $response->getHeaders(),
                            'message' => $response->getMessage()
                        ]
                    ]
                );

                return false;
            }

            $shop->setPaymentId($response->getData()['id']);
            $shop->setPaymentPassword($response->getData()['password']);

            /** @var string $clientWidgetCode */
            $clientWidgetCode =
                '<script>document.write(\'<iframe width="100%" height="470" frameborder="no" scrolling="" src="'
                . $this->appUrl
                . $this->router->generate(
                    'app.order.widget',
                    [
                        'insalesId' => $shop->getInsalesId(),
                        'widgetPassword' => $shop->getWidgetPassword(),
                    ]
                )
                . '/\'+window.order_info.id+\'">\')</script>';

            $client->createApplicationWidget(
                [
                    'code' => $clientWidgetCode,
                    'height' => '500'
                ]
            );

            $this->entityManager->persist($shop);
            $this->entityManager->flush();

            return $shop->getId() > 0;
        }

        /**
         * Удаление приложения
         *
         * @param Shop $shop
         *
         * @return bool
         */
        public function removeApplication(Shop $shop)
        {

            /** @var API\ApiClient $client */
            $client = new API\ApiClient(
                $this->appId,
                $shop->getPassword(),
                $shop->getShop()
            );

            foreach($client->getWebhooks()->getData() as $webHook)
                $client->removeWebhook($webHook['id']);

            $client->removePaymentGateway($shop->getPaymentId());

            /** @var Entity\Order[] $orders */
            $orders = $this->entityManager
                ->getRepository(Entity\Order::class)
                ->findBy(['shop' => $shop]);

            foreach($orders as $order)
                $this->entityManager->remove($order);

            $this->entityManager->flush();

            /** @var Entity\Deal[] $deals */
            $deals = $this->entityManager
                ->getRepository(Entity\Deal::class)
                ->findBy(['shop' => $shop]);

            foreach($deals as $deal)
                $this->entityManager->remove($deal);

            $this->entityManager->flush();

            /** @var Entity\DealEvent[] $dealEvents */
            $dealEvents = $this->entityManager
                ->getRepository(Entity\DealEvent::class)
                ->findBy(['shop' => $shop]);

            foreach($dealEvents as $dealEvent)
                $this->entityManager->remove($dealEvent);

            $this->entityManager->flush();

            /** @var Entity\Configuration $configuration */
            $configuration = $this->entityManager
                ->getRepository(Entity\Configuration::class)
                ->findOneBy(['shop' => $shop]);

            if($configuration instanceof Entity\Configuration)
                $this->entityManager->remove($configuration);

            $this->entityManager->flush();

            /** @var Entity\RequestLog[] $logs */
            $logs = $this->entityManager
                ->getRepository(Entity\RequestLog::class)
                ->findBy(['shop' => $shop]);

            foreach($logs as $log)
                $this->entityManager->remove($log);

            $this->entityManager->flush();

            /** @var Entity\Status[] $statuses */
            $statuses = $this->entityManager
                ->getRepository(Entity\Status::class)
                ->findBy(['shop' => $shop]);

            foreach($statuses as $status)
                $this->entityManager->remove($status);

            $this->entityManager->flush();

            /** @var Entity\TrackNumber[] $trackNumbers */
            $trackNumbers = $this->entityManager
                ->getRepository(Entity\TrackNumber::class)
                ->getByShop($shop);

            foreach($trackNumbers as $trackNumber)
                $this->entityManager->remove($trackNumber);

            $this->entityManager->flush();

            $this->entityManager->remove($shop);
            $this->entityManager->flush();

            return true;
        }
    }