<?php

    namespace App\Services\Insales;

    use App\Entity;
    use \InSales\API;
    use Doctrine\ORM\EntityManagerInterface;

    /**
     * Class Order
     *
     * @package App\Services\Insales
     */
    class Orders
    {

        /** @var array $clients */
        protected $clients = [];
        /** @var string $appId */
        protected $appId;
        /** @var \Doctrine\ORM\EntityManagerInterface $entityManager */
        protected $entityManager;

        /**
         * Order constructor.
         *
         * @param string $appId
         * @param \Doctrine\ORM\EntityManagerInterface $entityManager
         */
        public function __construct($appId, EntityManagerInterface $entityManager)
        {
            $this->appId = $appId;
            $this->entityManager = $entityManager;
        }

        /**
         * Загрузка заказа
         *
         * @param Entity\Shop $shop
         * @param int $orderId
         *
         * @return \App\Entity\Order|null
         */
        public function downloadOrder(Entity\Shop $shop, int $orderId) : ?Entity\Order
        {

            /** @var Entity\Order|null $order */
            $order = null;

            /** @var API\ApiClient $client */
            $client = new API\ApiClient($this->appId, $shop->getPassword(), $shop->getShop());

            /** @var API\ApiResponse $response */
            $response = $client->getOrderById($orderId);

            if(!$response->isSuccessful())
                return null;

            /** @var array $fields */
            $fields = $response->getData();

            /** @var Entity\Order $order */
            $order = $this->entityManager->getRepository(Entity\Order::class)
                ->findOneBy(
                [
                    'shop' => $shop,
                    'orderId' => $orderId
                ]
            );

            if (!$order instanceof Entity\Order)
            {

                /** @var Entity\Order $order */
                $order = new Entity\Order();
                $order->setShop($shop);
            }

            try
            {

                $order->setIsUpdateDeal(
                    $this->isUpdateDeal($order, $fields)
                );

                $order->setOrderId($fields['id'])
                    ->setOrderCreateAt(new \DateTime($fields['created_at']))
                    ->setOrderUpdateAt(new \DateTime($fields['updated_at']))
                    ->setOrderKey($fields['key'])
                    ->setOrderNumber($fields['number'])
                    ->setProducts($fields['order_lines'])
                    ->setPriceOrder($fields['total_price'])
                    ->setPriceDelivery($fields['full_delivery_price'])
                    ->setPriceItems($fields['items_price'])
                    ->setLocation($fields['shipping_address']['location'])
                    ->setClient($fields['client'])
                    ->setOrderComment($fields['comment'] ?? '')
                    ->setPayed($fields['financial_status'] === 'paid')
                    ->setPaymentId($fields['payment_gateway_id'])
                    ->setDeliveryTitle($fields['delivery_title'] ?? '')
                    ->setDeliveryDescription($fields['delivery_description'] ?? '')
                    ->setTransactionIid($fields['client_transaction_id'] ?? 0)
                    ->setStatus($fields['fulfillment_status'] ?? '')
                    ->setCustomStatus($fields['custom_status'] ?? [])
                ;

                $this->entityManager->persist($order);
                $this->entityManager->flush();
            }
            catch (\Exception $e)
            {
                return null;
            }

            return $order;
        }

        /**
         * Проверка обновления сделки
         * @param Entity\Order $order
         * @param array $fields
         * @return bool
         */
        public function isUpdateDeal(Entity\Order $order, array $fields)
        {

            return $order->getPriceOrder() != $fields['total_price']
                || $order->getPriceDelivery() != $fields['full_delivery_price']
                || $order->getPriceItems() != $fields['items_price']
                || (int) \count(self::diff_recursive($order->getProducts(), $fields['order_lines'])) > 0
            ;
        }

        public static function diff_recursive($array1, $array2)
        {

            $difference = [];

            foreach($array1 as $key => $value)
            {

                if(\is_array($value) && isset($array2[$key]))
                {

                    $new_diff = self::diff_recursive($value, $array2[$key]);

                    if( !empty($new_diff) )
                        $difference[$key] = $new_diff;
                }
                else if(\is_string($value) && !in_array($value, $array2))
                {
                    $difference[$key] = $value . " is missing from the second array";
                }
                else if(!is_numeric($key) && !array_key_exists($key, $array2))
                {
                    $difference[$key] = "Missing from the second array";
                }
            }

            return $difference;
        }

        /**
         * Получение заказа
         * @param Entity\Shop $shop
         * @param int $orderId
         * @return Entity\Order|null
         */
        public function getOrderByInsaslesId(Entity\Shop $shop, int $orderId) :?Entity\Order
        {

            /** @var Entity\Order $order */
            $order = $this->entityManager->getRepository(Entity\Order::class)->findOneBy(
                [
                    'shop' => $shop,
                    'orderId' => $orderId
                ]
            );

            if($order instanceof Entity\Order)
                return $order;

            return $this->downloadOrder($shop, $orderId);
        }
    }