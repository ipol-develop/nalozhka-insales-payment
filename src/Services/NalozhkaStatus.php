<?php

    namespace App\Services;

    use App\Entity;
    use Doctrine\ORM\EntityManagerInterface;

    /**
     * Class NalozhkaStatus
     *
     * @package App\Services
     */
    class NalozhkaStatus
    {

        /** @var \Doctrine\ORM\EntityManagerInterface $eventManager */
        protected $eventManager;
        /** @var \App\Repository\NalozhkaStatusRepository|\Doctrine\Common\Persistence\ObjectRepository $repository */
        protected $repository;

        /**
         * NalozhkaStatus constructor.
         *
         * @param \Doctrine\ORM\EntityManagerInterface $entityManager
         */
        public function __construct(EntityManagerInterface $entityManager)
        {
            $this->eventManager = $entityManager;
            $this->repository = $this->eventManager->getRepository(Entity\NalozhkaStatus::class);
        }

        /**
         * @param \App\Entity\Deal $deal
         *
         * @return object|null|Entity\NalozhkaStatus
         */
        public function getByDeal(Entity\Deal $deal)
        {
            return $this->repository->getByDeal($deal);
        }
    }