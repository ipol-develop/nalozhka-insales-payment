<?php

    namespace App\Services;

    use Doctrine\ORM\EntityManagerInterface;
    use App\Entity;

    /**
     * Class Statuses
     *
     * @package App\Services
     */
    class Statuses
    {

        /** @var \Doctrine\ORM\EntityManagerInterface $entityManager */
        protected $entityManager;

        /**
         * Configuration constructor.
         *
         * @param \Doctrine\ORM\EntityManagerInterface $entityManager
         */
        public function __construct(EntityManagerInterface $entityManager)
        {
            $this->entityManager = $entityManager;
        }

        /**
         * Получение настроек статусов
         *
         * @param \App\Entity\Shop $shop
         * @param array $insales
         * @param array $nalozhka
         *
         * @return array
         */
        public function getByShop(Entity\Shop $shop, array $insales, array $nalozhka)
        {
            return $this->entityManager
                ->getRepository(Entity\Status::class)
                ->getByShop($shop, $insales, $nalozhka)
            ;
        }

        /**
         * Сохранение настроек статусов
         *
         * @param \App\Entity\Shop $shop
         * @param array $statuses
         */
        public function setByShop(Entity\Shop $shop, array $statuses = [])
        {
            $this->entityManager
                ->getRepository(Entity\Status::class)
                ->setByShop($shop, $statuses)
            ;
        }
    }