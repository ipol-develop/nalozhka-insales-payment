<?php

    namespace App\Services;

    use Doctrine\ORM\EntityManagerInterface;
    use App\Entity;

    /**
     * Class DealEvent
     *
     * @package App\Services
     */
    class DealEvent
    {

        /** @var \Doctrine\ORM\EntityManagerInterface $eventManager */
        protected $eventManager;

        /**
         * DealEvent constructor.
         *
         * @param \Doctrine\ORM\EntityManagerInterface $entityManager
         */
        public function __construct(EntityManagerInterface $entityManager)
        {
            $this->eventManager = $entityManager;
        }

        /**
         * Создание события смены статуса заказа
         *
         * @param \App\Entity\Deal $deal
         * @param array $fields
         *
         * @return \App\Entity\DealEvent
         * @throws \Exception
         */
        public function changeDealStatus(Entity\Deal $deal, array $fields)
        {

            /** @var Entity\DealEvent $event */
            $event = new Entity\DealEvent();

            $event->setDeal($deal)
                ->setOrder($deal->getOrder())
                ->setShop($deal->getShop())
                ->setEventId($fields['id'])
                ->setEventType($fields['type'])
                ->setNewStatus($fields['new_status'])
                ->setOldStatus($fields['old_status'])
                ->setOccurredAt(new \DateTime($fields['occurred_at']))
            ;

            $this->eventManager->persist($event);
            $this->eventManager->flush();

            return $event;
        }

        /**
         * Создание события изменения сделки
         *
         * @param \App\Entity\Deal $deal
         * @param array $fields
         *
         * @return \App\Entity\DealEvent
         * @throws \Exception
         */
        public function changeDeal(Entity\Deal $deal, array $fields)
        {

            /** @var Entity\DealEvent $event */
            $event = new Entity\DealEvent();

            $event->setDeal($deal)
                ->setOrder($deal->getOrder())
                ->setShop($deal->getShop())
                ->setEventId($fields['id'])
                ->setEventType($fields['type'])
                ->setNewStatus($fields['deal']['status'])
                ->setOldStatus($fields['deal']['status'])
                ->setOccurredAt(new \DateTime($fields['occurred_at']))
            ;

            $this->eventManager->persist($event);
            $this->eventManager->flush();

            return $event;
        }
    }