<?php

    namespace App\Services;

    use Doctrine\ORM\EntityManagerInterface;
    use App\Entity;

    /**
     * Class Deal
     *
     * @package App\Services
     */
    class Deal
    {
        /**
         * @var \Doctrine\ORM\EntityManagerInterface
         */
        protected $entityManager;
        /**
         * @var \App\Repository\DealRepository|\Doctrine\Common\Persistence\ObjectRepository
         */
        protected $repository;


        /**
         * Deal constructor.
         *
         * @param \Doctrine\ORM\EntityManagerInterface $entityManager
         */
        public function __construct(EntityManagerInterface $entityManager)
        {
            $this->entityManager = $entityManager;
            $this->repository = $this->entityManager->getRepository(Entity\Deal::class);
        }

        /**
         * Получение сделки по магазину / заказу
         *
         * @param $order
         *
         * @return object|null|Entity\Deal
         */
        public function getByShopOrder(Entity\Order $order)
        {
            return $this->repository->getByShopOrder($order);
        }

        /**
         * Получение сделки по внешнему id сделки
         *
         * @param int $id
         *
         * @return \App\Entity\Deal|object|null
         */
        public function getByDealId(int $id)
        {
            return $this->repository->getByDealId($id);
        }

        /**
         * Сохранение сделки
         *
         * @param \App\Entity\Deal $deal
         */
        public function saveDeal(Entity\Deal $deal)
        {
            $this->entityManager->persist($deal);
            $this->entityManager->flush();
        }

        /**
         * Удаление сделки
         *
         * @param \App\Entity\Deal $deal
         */
        public function removeDeal(Entity\Deal $deal)
        {
            $this->entityManager->remove($deal);
            $this->entityManager->flush();
        }
    }