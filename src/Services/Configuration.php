<?php

    namespace App\Services;

    use Doctrine\ORM\EntityManagerInterface;
    use App\Entity;

    /**
     * Class Configuration
     *
     * @package App\Services
     */
    class Configuration
    {

        /** @var \Doctrine\ORM\EntityManagerInterface $entityManager */
        protected $entityManager;

        /**
         * Configuration constructor.
         *
         * @param \Doctrine\ORM\EntityManagerInterface $entityManager
         */
        public function __construct(EntityManagerInterface $entityManager)
        {
            $this->entityManager = $entityManager;
        }

        /**
         * Сохранение конфигураций
         *
         * @param \App\Entity\Shop $shop
         * @param \App\Entity\Configuration $configuration
         */
        public function setConfigurationByShop(Entity\Shop $shop, Entity\Configuration $configuration)
        {
            return $this->entityManager
                ->getRepository(Entity\Configuration::class)
                ->setByShop($shop, $configuration);
        }
    }