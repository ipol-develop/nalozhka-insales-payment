<?php

    namespace App\Services;

    use Doctrine\ORM\EntityManagerInterface;
    use App\Entity;

    /**
     * Class TrackNumber
     *
     * @package App\Services
     */
    class TrackNumber
    {

        /** @var \Doctrine\ORM\EntityManagerInterface $entityManager */
        protected $entityManager;
        /** @var \App\Repository\TrackNumberRepository|\Doctrine\Common\Persistence\ObjectRepository $repository */
        protected $repository;

        /**
         * TrackNumber constructor.
         *
         * @param \Doctrine\ORM\EntityManagerInterface $entityManager
         */
        public function __construct(EntityManagerInterface $entityManager)
        {
            $this->entityManager = $entityManager;
            $this->repository = $this->entityManager->getRepository(Entity\TrackNumber::class);
        }

        /**
         * @param \App\Entity\Shop $shop
         * @param \App\Entity\Order $order
         *
         * @return \App\Entity\TrackNumber[]|null
         */
        public function getByShopOrder(Entity\Shop $shop, Entity\Order $order)
        {
            return $this->repository->getByShopOrder($shop, $order);
        }

        /**
         * @param \App\Entity\Shop $shop
         * @param \App\Entity\Order $order
         * @param int $id
         *
         * @return \App\Entity\TrackNumber|object|null
         */
        public function getByShopOrderTrackId(Entity\Shop $shop, Entity\Order $order, int $id)
        {
            return $this->repository->getByShopOrderTrackId($shop, $order, $id);
        }

        /**
         * @param \App\Entity\TrackNumber $trackNumber
         */
        public function createByTrackingNumber(Entity\TrackNumber $trackNumber)
        {
            $this->entityManager->persist($trackNumber);
            $this->entityManager->flush();
        }

        /**
         * @param \App\Entity\TrackNumber $trackNumber
         */
        public function removeByTrackingNumber(Entity\TrackNumber $trackNumber)
        {
            $this->entityManager->remove($trackNumber);
            $this->entityManager->flush();
        }

        /**
         * @param \App\Entity\Shop $shop
         * @param \App\Entity\Order $order
         * @param \App\Entity\Deal $deal
         * @param string $provider
         * @param string $trackNumber
         *
         * @return \App\Entity\TrackNumber
         */
        public function createTracking(Entity\Shop $shop, Entity\Order $order, Entity\Deal $deal, string $provider, string $trackNumber)
        {

            /** @var Entity\TrackNumber $number */
            $number = new Entity\TrackNumber();

            $number->setShop($shop)
                ->setOrder($order)
                ->setDeal($deal)
                ->setProvider($provider)
                ->setTrackNumber($trackNumber)
            ;

            return $number;
        }
    }