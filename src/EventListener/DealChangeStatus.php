<?php

    namespace App\EventListener;

    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    use App\Event;

    /**
     * Class DealChangeStatus
     * @package App\EventListener
     */
    class DealChangeStatus implements EventSubscriberInterface
    {

        /**
         * Returns an array of event names this subscriber wants to listen to.
         *
         * The array keys are event names and the value can be:
         *
         *  * The method name to call (priority defaults to 0)
         *  * An array composed of the method name to call and the priority
         *  * An array of arrays composed of the method names to call and respective
         *    priorities, or 0 if unset
         *
         * For instance:
         *
         *  * ['eventName' => 'methodName']
         *  * ['eventName' => ['methodName', $priority]]
         *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
         *
         * @return array The event names to listen to
         */
        public static function getSubscribedEvents()
        {
            return [
                Event\DealChangeStatus::NAME => [
                    'updateStatus',
                    10
                ]
            ];
        }

        /** @var EntityManagerInterface $entityManager */
        protected $entityManager;

        /**
         * DealChangeStatusListener constructor.
         * @param EntityManagerInterface $entityManager
         */
        public function __construct(EntityManagerInterface $entityManager)
        {
            $this->entityManager = $entityManager;
        }

        /**
         * @param Event\DealChangeStatus $event
         */
        public function updateStatus(Event\DealChangeStatus $event)
        {

            /** @var \App\Entity\Deal $deal */
            $deal = $event->getDealEvent()->getDeal();
            $deal->setStatus($event->getDealEvent()->getNewStatus());

            $this->entityManager->persist($deal);
            $this->entityManager->flush();
        }
    }