<?php

    namespace App\EventListener;

    use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
    use Symfony\Component\HttpFoundation\RedirectResponse;
    use Symfony\Component\HttpKernel\Event\GetResponseEvent;
    use Symfony\Component\Routing\RouterInterface;

    /**
     * Class RequestListener
     *
     * @package App\EventListener
     */
    class Request
    {

        /** @var \Symfony\Component\Routing\RouterInterface $router */
        protected $router;
        /** @var ParameterBagInterface $parameterBag */
        protected $parameterBag;

        /**
         * RequestListener constructor.
         *
         * @param \Symfony\Component\Routing\RouterInterface $router
         * @param ParameterBagInterface $parameterBag
         */
        public function __construct(RouterInterface $router, ParameterBagInterface $parameterBag)
        {
            $this->router = $router;
            $this->parameterBag = $parameterBag;
        }

        /**
         * Проверка авторизации пользователя
         *
         * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
         */
        public function checkUserAuth(GetResponseEvent $event)
        {

            /** @var int $insalesId */
            $insalesId = (int) $event->getRequest()->getSession()->get('insalesId');

            if(
                $insalesId === 0
                && \in_array(
                    $event->getRequest()->attributes->get('_route'),
                    $this->parameterBag->get('no_auth_url')
                )
            )
            {
                $event->setResponse(
                    new RedirectResponse(
                        $this->router->generate('session.end')
                    )
                );
            }
        }
    }