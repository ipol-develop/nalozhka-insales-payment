<?php

    namespace App\EventListener;

    use App\Services\Deal;
    use App\Services\Nalozhka\Client;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    use App\Event;
    use App\Entity;

    /**
     * Class OrderUpdateDeal
     *
     * @package App\EventListener
     */
    class OrderUpdateDeal implements EventSubscriberInterface
    {

        /** @var EntityManagerInterface $entityManager */
        protected $entityManager;
        /** @var Client $client */
        protected $client;
        /** @var \App\Services\Deal $deal */
        protected $deal;

        /**
         * Returns an array of event names this subscriber wants to listen to.
         *
         * The array keys are event names and the value can be:
         *
         *  * The method name to call (priority defaults to 0)
         *  * An array composed of the method name to call and the priority
         *  * An array of arrays composed of the method names to call and respective
         *    priorities, or 0 if unset
         *
         * For instance:
         *
         *  * ['eventName' => 'methodName']
         *  * ['eventName' => ['methodName', $priority]]
         *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
         *
         * @return array The event names to listen to
         */
        public static function getSubscribedEvents()
        {
            return [
                Event\OrderUpdateDeal::NAME => [
                    'updateDeal',
                    10
                ]
            ];
        }

        /**
         * OrderCancel constructor.
         *
         * @param EntityManagerInterface $entityManager
         * @param Client $client
         * @param \App\Services\Deal $deal
         */
        public function __construct(EntityManagerInterface $entityManager, Client $client, Deal $deal)
        {
            $this->entityManager = $entityManager;
            $this->client = $client;
            $this->deal = $deal;
        }

        /**
         * @param Event\OrderUpdateDeal $event
         * @return bool
         */
        public function updateDeal(Event\OrderUpdateDeal $event)
        {

            /** @var Entity\Deal $deal */
            $deal = $this->deal->getByShopOrder($event->getOrder());

            if(!$deal instanceof Entity\Deal)
                return false;

            $this->deal->removeDeal($deal);

            return true;
        }
    }