<?php

    namespace App\EventListener;

    use App\Services\Insales\Client;
    use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    use App\Event;

    /**
     * Class OrderPayed
     *
     * @package App\EventListener
     */
    class OrderPayed implements EventSubscriberInterface
    {

        /** @var \App\Services\Insales\Client $insalesClient */
        protected $insalesClient;
        /** @var \App\Services\Nalozhka\Client $nalozhkaClient */
        protected $nalozhkaClient;
        /** @var \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface $parameterBag */
        protected $parameterBag;

        /**
         * Returns an array of event names this subscriber wants to listen to.
         *
         * The array keys are event names and the value can be:
         *
         *  * The method name to call (priority defaults to 0)
         *  * An array composed of the method name to call and the priority
         *  * An array of arrays composed of the method names to call and respective
         *    priorities, or 0 if unset
         *
         * For instance:
         *
         *  * ['eventName' => 'methodName']
         *  * ['eventName' => ['methodName', $priority]]
         *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
         *
         * @return array The event names to listen to
         */
        public static function getSubscribedEvents()
        {
            return [
                Event\OrderPayed::NAME => [
                    'payedOrder',
                    10
                ]
            ];
        }

        /**
         * @param Client $insalesClient
         * @param \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface $parameterBag
         */
        public function __construct(Client $insalesClient, ParameterBagInterface $parameterBag)
        {
            $this->insalesClient = $insalesClient;
            $this->parameterBag = $parameterBag;
        }

        /**
         * @param Event\OrderPayed $event
         * @return bool
         */
        public function payedOrder(Event\OrderPayed $event)
        {

            $this->insalesClient->setShop($event->getShop());

            /** @var \InSales\API\ApiResponse $response */
            $response = $this->insalesClient->setOrderPayed($event->getOrder(), true);

            return $response->isSuccessful();
        }
    }