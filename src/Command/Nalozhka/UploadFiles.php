<?php

    namespace App\Command\Nalozhka;

    use App\Entity;
    use App\Services;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
    use Symfony\Component\HttpFoundation\Response;

    /**
     * Class UploadFiles
     *
     * @package App\Command\Nalozhka
     *
     * Выгрузка фотографий в сделку
     */
    class UploadFiles extends Command
    {

        /** @var int Количество сделак за шаг */
        const UPLOAD_DEAL_COUNT = 1;

        /** @var EntityManagerInterface $entityManager */
        protected $entityManager;
        /** @var Services\Nalozhka\Client $nalozhka */
        protected $nalozhka;
        /** @var Services\Insales\Client $insales */
        protected $insales;
        /** @var ParameterBagInterface $parameterBag */
        protected $parameterBag;

        /**
         * UploadFiles constructor.
         *
         * @param EntityManagerInterface $entityManager
         * @param \App\Services\Nalozhka\Client $nalozhka
         * @param \App\Services\Insales\Client $insales
         * @param ParameterBagInterface $parameterBag
         */
        public function __construct(
            EntityManagerInterface $entityManager,
            Services\Nalozhka\Client $nalozhka,
            Services\Insales\Client $insales,
            ParameterBagInterface $parameterBag
        )
        {

            parent::__construct('app:nalozhka:deal:photo-upload');

            $this->nalozhka = $nalozhka;
            $this->insales = $insales;
            $this->entityManager = $entityManager;
            $this->parameterBag = $parameterBag;
        }

        /**
         * @param Entity\Shop $shop
         * @return Entity\Configuration|object|null
         */
        protected function getConfigurationByShop(Entity\Shop $shop)
        {
            return $this->entityManager
                ->getRepository(Entity\Configuration::class)
                ->getByShop($shop);
        }

        /**
         * @param InputInterface $input
         * @param OutputInterface $output
         * @return int|void|null
         */
        protected function execute(InputInterface $input, OutputInterface $output)
        {

            /** @var Entity\Deal[] $deals */
            $deals = $this->entityManager
                ->getRepository(Entity\Deal::class)
                ->getOrderExportPhotos($this->parameterBag->get('nalozhka.file.upload.limit'));

            foreach($deals as $deal)
            {

                /** @var Entity\Configuration $configuration */
                $configuration = $this->getConfigurationByShop($deal->getShop())->setShop($deal->getShop());

                $this->nalozhka
                    ->setShop($deal->getShop())
                    ->setConfiguration($configuration);

                $this->insales
                    ->setShop($deal->getShop())
                    ->setConfiguration($configuration);

                /** @var Entity\Order $order */
                $order = $deal->getOrder();

                /** @var array $uploadFiles */
                $uploadFiles = [];

                foreach($order->getProducts() as $product)
                {

                    /** @var array $images */
                    $images = $this->insales->getProductImage($product['product_id']);

                    /** @var array $image */
                    foreach($images as $image)
                    {

                        if(empty($image['original_url']))
                            continue;

                        $fileHeaders = \get_headers($image['original_url'], true);

                        /** @var \App\Services\Nalozhka\ApiResponse $response */
                        $response = $this->nalozhka->uploadFile(
                            [
                                'filename' => $image['filename'],
                                'file' => $image['original_url'],
                                'fileSize' => $fileHeaders['Content-Length'],
                                'header' => [
                                    'Content-Type: ' . $fileHeaders['Content-Type']
                                ]
                            ]
                        );

                        if(!$response->isSuccessful())
                            continue;

                        $uploadFiles[] = [
                            'id' => $response->getData()['~id'],
                            'description' => $image['filename']
                        ];
                    }
                }

                if(\count($uploadFiles) > 0)
                {

                    /** @var \App\Services\Nalozhka\ApiResponse $response */
                    $response = $this->nalozhka->appendFileToDeal($deal, $uploadFiles);

                    if($response->isSuccessful())
                    {
                        $order->setFileUploaded(true);
                        $this->entityManager->flush();
                    }
                }
                else
                {
                    $order->setFileUploaded(true);
                    $this->entityManager->flush();
                }
            }
        }
    }