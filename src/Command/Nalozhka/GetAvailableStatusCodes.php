<?php

    namespace App\Command\Nalozhka;

    use App\Entity;
    use App\Services\Nalozhka\Client;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

    /**
     * Class DownloadStatusesCode
     *
     * @package App\Command\Nalozhka
     *
     * Получение статусов сделок из наложки
     */
    class GetAvailableStatusCodes extends Command
    {

        /** @var \Doctrine\ORM\EntityManagerInterface $entityManager */
        protected $entityManager;
        /** @var \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface $parameterBag */
        protected $parameterBag;
        /** @var \App\Services\Nalozhka\Client $client */
        protected $nalozhka;

        /**
         * DownloadStatusesCode constructor.
         *
         * @param \Doctrine\ORM\EntityManagerInterface $entityManager
         * @param \App\Services\Nalozhka\Client $nalozhka
         * @param \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface $parameterBag
         */
        public function __construct(
            EntityManagerInterface $entityManager,
            Client $nalozhka,
            ParameterBagInterface $parameterBag
        )
        {

            parent::__construct('app:nalozhka:statuses-download');

            $this->entityManager = $entityManager;
            $this->parameterBag = $parameterBag;
            $this->nalozhka = $nalozhka;
        }

        /**
         * @param \Symfony\Component\Console\Input\InputInterface $input
         * @param \Symfony\Component\Console\Output\OutputInterface $output
         *
         * @return int|void|null
         */
        protected function execute(InputInterface $input, OutputInterface $output)
        {

            $configuration = new Entity\Configuration();
            $configuration->setApiKeyTest($this->parameterBag->get('app.nalozhka.test.auth'))
                ->setTestMode(true);

            $this->nalozhka->setConfiguration($configuration);

            Client::$noLog = true;

            foreach($this->nalozhka->getStatuses() as $status)
            {
                $this->entityManager->getRepository(Entity\NalozhkaStatus::class)
                    ->updateStatus($status['id'], $status['title']);
            }
        }
    }