<?php

    namespace App\Command\Insales;

    use App\Entity;
    use App\Services\Insales\Client;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

    /**
     * Class StatusesUpdate
     *
     * @package App\Command\Insales
     *
     * Обновление статусов заказов insales относительно статусов сделки
     */
    class UpdateInsalesOrdersStatuses extends Command
    {

        /** @var EntityManagerInterface $entityManager */
        protected $entityManager;
        /** @var ParameterBagInterface $parameterBag */
        protected $parameterBag;
        /** @var Client $client */
        protected $insales;

        /**
         * StatusesUpdate constructor.
         *
         * @param EntityManagerInterface $entityManager
         * @param ParameterBagInterface $parameterBag
         * @param \App\Services\Insales\Client $insales
         */
        public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $parameterBag, Client $insales)
        {

            parent::__construct('app:insales:statuses-update');

            $this->entityManager = $entityManager;
            $this->parameterBag = $parameterBag;
            $this->insales = $insales;
        }

        /**
         * @param InputInterface $input
         * @param OutputInterface $output
         * @return int|void|null
         */
        protected function execute(InputInterface $input, OutputInterface $output)
        {

            /** @var Entity\DealEvent[] $dealEvents */
            $dealEvents = $this->entityManager
                ->getRepository(Entity\DealEvent::class)
                ->getExportStatuses($this->parameterBag->get('insales.statuses.update.limit'));

            /** @var Entity\DealEvent $dealEvent */
            foreach($dealEvents as $dealEvent)
            {

                $dealEvent->setExported(true);
                $this->entityManager->flush();

                /** @var Entity\Configuration $configuration */
                $configuration = $this->entityManager
                    ->getRepository(Entity\Configuration::class)
                    ->getByShop($dealEvent->getShop());


                if(!$configuration instanceof Entity\Configuration)
                    continue;

                if(!$configuration->isUpdateStatuses())
                    continue;

                $this->insales
                    ->setShop($dealEvent->getShop())
                    ->setConfiguration($configuration);

                /** @var Entity\Status $status */
                $status = $this->entityManager
                    ->getRepository(Entity\Status::class)
                    ->getShopStatusByCode($dealEvent->getShop(), $dealEvent->getNewStatus());

                if(!$status instanceof Entity\Status)
                    continue;

                if($status->getInsales() === '')
                    continue;

                if($configuration->isUseCustomStatuses())
                {
                    $this->insales->setOrderCustomStatus($dealEvent->getOrder()->getOrderId(), $status->getInsales());
                }
                else
                {
                    $this->insales->setOrderStatus($dealEvent->getOrder()->getOrderId(), $status->getInsales());
                }
            }
        }
    }