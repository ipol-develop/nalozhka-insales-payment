<?php

    namespace App\Repository;

    use Doctrine\ORM\EntityRepository;
    use App\Entity;

    /**
     * Class Deal
     * @package App\Repository
     */
    class DealRepository extends EntityRepository
    {

        /**
         * Получение сделки по заказу
         * @param Entity\Order $order
         * @return object|null
         */
        public function getByShopOrder(Entity\Order $order)
        {
            return $this->findOneBy(
                [
                    'shop' => $order->getShop(),
                    'order' => $order
                ]
            );
        }

        /**
         * Получение сделки по externalId
         * @param int $id
         * @return Entity\Deal|object|null
         */
        public function getByDealId(int $id)
        {
            return $this->findOneBy(
                [
                    'dealId' => $id
                ]
            );
        }

        /**
         * Получение сделок для которых нужно выгрузить фотографии
         * @param int $limit
         * @return \App\Entity\Deal[]|null
         */
        public function getOrderExportPhotos($limit = 3)
        {

            $query = $this->createQueryBuilder('deal');

            $query->innerJoin('deal.order', 'orders')
                ->where('orders.fileUploaded = :fileUpload')
                ->setParameters(
                    [
                        'fileUpload' => false,
                    ]
                )
                ->setMaxResults($limit)
            ;

            return $query->getQuery()->getResult();
        }
    }