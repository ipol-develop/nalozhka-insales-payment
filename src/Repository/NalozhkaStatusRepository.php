<?php

    namespace App\Repository;

    use Doctrine\ORM\EntityRepository;
    use \App\Entity;

    /**
     * Class NalozhkaStatus
     *
     * @package App\Repository
     */
    class NalozhkaStatusRepository extends EntityRepository
    {

        /**
         * Обновление статуса
         *
         * @param $code
         * @param $title
         *
         * @return bool
         */
        public function updateStatus($code, $title)
        {

            /** @var Entity\NalozhkaStatus $status */
            $status = $this->findOneBy(
                [
                    'code' => $code
                ]
            );

            if(!$status instanceof Entity\NalozhkaStatus)
            {
                $status = new Entity\NalozhkaStatus();
                $status->setCode($code);
            }

            $status->setTitle($title);

            try
            {
                $this->getEntityManager()->persist($status);
                $this->getEntityManager()->flush($status);

                return true;
            }catch(\Exception $exception)
            {
                return false;
            }
        }

        /**
         * Получение статуса сделки
         *
         * @param \App\Entity\Deal $deal
         *
         * @return object|null
         */
        public function getByDeal(Entity\Deal $deal)
        {
            return $this->findOneBy(
                [
                    'code' => $deal->getStatus(),
                ]
            );
        }
    }