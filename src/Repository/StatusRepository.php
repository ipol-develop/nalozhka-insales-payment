<?php

    namespace App\Repository;

    use Doctrine\ORM\EntityRepository;
    use App\Entity;
    use Doctrine\ORM\OptimisticLockException;
    use Doctrine\ORM\ORMException;

    /**
     * Class Statuses
     * @package App\Repository
     */
    class StatusRepository extends EntityRepository
    {

        /**
         * @param Entity\Shop $shop
         * @param array $insales
         * @param array $nalozhka
         * @return array
         */
        public function getByShop(Entity\Shop $shop, array $insales = [], array $nalozhka = [])
        {

            /** @var array $result */
            $result = [];

            foreach($nalozhka as $nalozhkaStatus)
            {

                /** @var Entity\Status $status */
                $status = $this->findOneBy(
                    [
                        'shop' => $shop,
                        'nalozhka' => $nalozhkaStatus['id'],
                    ]
                );

                $result[$nalozhkaStatus['id']] = [
                    'id' => $nalozhkaStatus['id'],
                    'title' => $nalozhkaStatus['title'],
                    'statuses' => []
                ];

                foreach ($insales as $insaleStatus)
                {
                    $result[$nalozhkaStatus['id']]['statuses'][] = [
                        'permalink' => $insaleStatus['permalink'],
                        'title' => $insaleStatus['title'],
                        'color' => $insaleStatus['color'],
                        'selected' => $status instanceof Entity\Status && $status->getInsales() === $insaleStatus['permalink']
                    ];
                }
            }

            return $result;
        }

        /**
         * @param Entity\Shop $shop
         * @param array $statuses
         * @return bool
         */
        public function setByShop(Entity\Shop $shop, array $statuses = [])
        {

            foreach($statuses as $nalozhka => $insales)
            {

                /** @var Entity\Status $status */
                $status = $this->findOneBy(
                    [
                        'shop' => $shop,
                        'nalozhka' => $nalozhka,
                    ]
                );

                if(!$status instanceof Entity\Status)
                {

                    /** @var Entity\Status $status */
                    $status = new Entity\Status();
                    $status->setShop($shop)
                        ->setNalozhka($nalozhka);
                }

                $status->setInsales($insales);

                try
                {
                    $this->getEntityManager()->persist($status);
                }
                catch (ORMException $e)
                {}
            }

            try
            {
                $this->getEntityManager()->flush();
            }
            catch (OptimisticLockException $e)
            {}
            catch (ORMException $e)
            {}

            return true;
        }

        /**
         * Получение insales статуса под коду от наложки
         * @param Entity\Shop $shop
         * @param string $status
         * @return object|null
         */
        public function getShopStatusByCode(Entity\Shop $shop, string $status)
        {
            return $this->findOneBy(
                [
                    'shop' => $shop,
                    'nalozhka' => $status,
                ]
            );
        }
    }