<?php

    namespace App\Repository;

    use Doctrine\ORM\EntityRepository;

    /**
     * Class DealEvent
     * @package App\Repository
     */
    class DealEventRepository extends EntityRepository
    {

        /**
         * Получение обновлений статусов для выгрузки
         * @param int $limit
         * @return \App\Entity\DealEvent[]|array
         */
        public function getExportStatuses($limit = 100)
        {
            return $this->findBy(
                [
                    'exported' => false,
                ],
                [
                    'id' => 'asc',
                ],
                $limit
            );
        }
    }