<?php

    namespace App\Repository;

    use Doctrine\ORM\EntityRepository;
    use App\Entity;
    use Doctrine\ORM\ORMException;

    /**
     * Class RequestResponseLog
     * @package App\Repository
     */
    class RequestLogRepository extends EntityRepository
    {

        /**
         * @param \App\Entity\Shop $shop
         * @param string $url
         * @param string $method
         * @param array $request
         * @param array $response
         */
        public function saveLog(
            Entity\Shop $shop,
            string $url,
            string $method,
            array $request = [],
            array $response = []
        )
        {

            /** @var Entity\RequestLog $log */
            $log = new Entity\RequestLog();

            $log->setShop($shop)
                ->setUrl($url)
                ->setMethod($method)
                ->setRequest($request)
                ->setResponse($response)
            ;

            try
            {
                $this->getEntityManager()->persist($log);
                $this->getEntityManager()->flush($log);
            }
            catch(ORMException $e)
            {}
        }

        /**
         * Получение логов
         *
         * @param \App\Entity\Shop $shop
         *
         * @return \Doctrine\ORM\QueryBuilder
         */
        public function getLogs(Entity\Shop $shop)
        {

            $query = $this->createQueryBuilder('log');
            $query->andWhere('log.shop = :shop');
            $query->setParameter('shop', $shop);
            $query->orderBy('log.createAt', 'desc');

            return $query;
        }
    }