<?php

    namespace App\Repository;

    use Doctrine\ORM\EntityRepository;
    use App\Entity;
    use Doctrine\ORM\ORMException;

    /**
     * Class Configuration
     * @package App\Repository
     */
    class ConfigurationRepository extends EntityRepository
    {

        /**
         * Получение настроек по магазину
         * @param \App\Entity\Shop $shop
         * @return object|null|\App\Entity\Configuration
         */
        public function getByShop(Entity\Shop $shop)
        {

            /** @var \App\Entity\Configuration $configuration */
            $configuration = $this->findOneBy(
                [
                    'shop' => $shop,
                ]
            );

            if($configuration instanceof Entity\Configuration)
                return $configuration;

            $configuration = new Entity\Configuration();
            $configuration->setShop($shop);

            return $configuration;
        }

        /**
         * @param $shop
         * @param Entity\Configuration $configuration
         */
        public function setByShop($shop, Entity\Configuration $configuration)
        {

            $configuration->setShop($shop);

            try
            {
                $this->getEntityManager()->persist($configuration);
                $this->getEntityManager()->flush($configuration);
            }
            catch (ORMException $e)
            {
            }
        }
    }