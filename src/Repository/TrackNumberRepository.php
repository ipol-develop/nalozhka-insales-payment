<?php

    namespace App\Repository;

    use App\Entity;
    use Doctrine\ORM\EntityRepository;

    /**
     * Class TrackNumber
     *
     * @package App\Repository
     */
    class TrackNumberRepository extends EntityRepository
    {

        /**
         * Получение треков по магазину
         *
         * @param \App\Entity\Shop $shop
         *
         * @return \App\Entity\TrackNumber[]|array
         */
        public function getByShop(Entity\Shop $shop)
        {
            return $this->findBy(
                [
                    'shop' => $shop,
                ]
            );
        }

        /**
         * Получение треков для заказа
         *
         * @param \App\Entity\Shop $shop
         * @param \App\Entity\Order $order
         *
         * @return \App\Entity\TrackNumber[]|null
         */
        public function getByShopOrder(Entity\Shop $shop, Entity\Order $order)
        {
            return $this->findBy(
                [
                    'shop' => $shop,
                    'order' => $order,
                ]
            );
        }

        /**
         * Получение трека по магазину, ордеру, id
         *
         * @param \App\Entity\Shop $shop
         * @param \App\Entity\Order $order
         *
         * @param int $id
         *
         * @return \App\Entity\TrackNumber|null|object
         */
        public function getByShopOrderTrackId(Entity\Shop $shop, Entity\Order $order, int $id)
        {
            return $this->findOneBy(
                [
                    'shop' => $shop,
                    'order' => $order,
                    'id' => $id
                ]
            );
        }
    }