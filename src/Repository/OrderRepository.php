<?php

    namespace App\Repository;

    use Doctrine\ORM\EntityRepository;

    /**
     * Class Order
     * @package App\Repository
     */
    class OrderRepository extends EntityRepository
    {

        /**
         * Получение заказов с не выгруженными файлами
         * @param $limit
         * @return array
         */
        public function getUploadFiles($limit)
        {
            return $this->findBy(
                [
                    'fileUploaded' => false,
                ],
                [
                    'id' => 'asc',
                ],
                $limit
            );
        }
    }