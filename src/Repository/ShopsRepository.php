<?php

    namespace App\Repository;

    use \Doctrine\ORM\EntityRepository;

    /**
     * Class Shops
     *
     * @package App\Repository
     */
    class ShopsRepository extends EntityRepository
    {

        /**
         * Получение магазина по insalesId
         *
         * @param int $insalesId
         *
         * @return \App\Entity\Shop|object|null
         */
        public function getByInsalesId(int $insalesId)
        {
            return $this->findOneBy(
                [
                    'insalesId' => $insalesId
                ]
            );
        }
    }