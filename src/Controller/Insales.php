<?php

    namespace App\Controller;

    use App\Services\Insales\Installer;
    use App\Entity;
    use App\Services\Insales\User;
    use Psr\Log\LoggerInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;

    /**
     * Class Insales
     *
     * @package App\Controller
     */
    class Insales extends Base
    {

        /**
         * Установка приложения
         *
         * @param \Symfony\Component\HttpFoundation\Request $request
         * @param \App\Services\Insales\Installer $installer
         *
         * @return \Symfony\Component\HttpFoundation\Response
         * @throws \Exception
         */
        public function installApplication(Installer $installer, Request $request)
        {

            /** @var Entity\Shop $shop */
            $shop = new Entity\Shop();

            $shop->setInsalesId($request->query->get('insales_id'))
                ->setShop($request->query->get('shop'))
                ->setPassword($installer->getPasswordFromToken($request->query->get('token')))
                ->setPasswordToken($request->query->get('token'))
            ;

            $shop->createWidgetPassword();

            /** @var bool $result */
            $result = $installer->installApplication($shop);

            return new Response(
                null,
                (bool) $result
                    ? Response::HTTP_OK
                    : Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        /**
         * Удаление приложения
         *
         * @param \App\Services\Insales\Installer $installer
         *
         * @param \Symfony\Component\HttpFoundation\Request $request
         * @param LoggerInterface $logger
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function removeApplication(Installer $installer, Request $request, LoggerInterface $logger)
        {

            /** @var Entity\Shop $shop */
            $shop = $this->getShopById($request->query->getInt('insales_id'));

            if(!$shop instanceof Entity\Shop)
            {

                $logger->error(
                    'error remove application, shop not found',
                    $request->query->all()
                );

                return new Response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            /** @var bool $result */
            $result = $installer->removeApplication($shop);

            return new Response(
                null,
                (bool) $result
                    ? Response::HTTP_OK
                    : Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        /**
         * Авторизация пользователя
         *
         * @param \App\Services\Insales\User $user
         *
         * @return mixed|\Symfony\Component\HttpFoundation\RedirectResponse
         */
        public function authInsalesUser(User $user)
        {

            $user->getAuthorize();

            if(!$user->isAuthorized())
                return $user->getReturn();

            return $this->redirectToRoute('app.configure');
        }
    }