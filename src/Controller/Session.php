<?php

    namespace App\Controller;

    use Symfony\Component\HttpFoundation\Request;

    /**
     * Class Session
     *
     * @package App\Controller
     */
    class Session extends Base
    {

        /**
         * @param \Symfony\Component\HttpFoundation\Request $request
         *
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function end(Request $request)
        {
            return $this->render('session/end.html.twig');
        }
    }