<?php

    namespace App\Controller;

    use Symfony\Component\HttpFoundation\Request;

    /**
     * Class ExceptionController
     *
     * @package App\Controller
     */
    class Exception extends Base
    {

        public function page404(Request $request)
        {

            /** @var \App\Entity\Shop $shop */
            $shop = $this->getShopById();

            return $this->render(
                'exception/404.html.twig',
                [
                    'shop' => $shop,
                ]
            );
        }
    }