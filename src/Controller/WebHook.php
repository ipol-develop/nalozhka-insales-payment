<?php

    namespace App\Controller;

    use App\Entity;
    use App\Event;
    use App\Services;
    use App\Services\Insales\Orders;
    use Psr\Log\LoggerInterface;
    use Symfony\Component\EventDispatcher\EventDispatcherInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;

    /**
     * Class WebHook
     *
     * @package App\Controller
     */
    class WebHook extends Base
    {

        /**
         * Загрузка информации о заказе
         *
         * @param \Symfony\Component\HttpFoundation\Request $request
         * @param \App\Services\Insales\Orders $orders
         * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
         *
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function order(Request $request, Orders $orders, EventDispatcherInterface $dispatcher)
        {

            /** @var Response $response */
            $response = new Response(null, Response::HTTP_OK);

            /** @var array $requestData */
            $requestData = \json_decode($request->getContent(), true);

            /** @var Entity\Shop $shop */
            $shop = $this->getShopById((int) $requestData['account_id']);

            if(!$shop instanceof Entity\Shop)
                return $response;

            /** @var Entity\Order $order */
            $order = $orders->downloadOrder($shop, $requestData['id']);

            /** @var Entity\Configuration $configuration */
            $configuration = $this->getConfigurationByShop($shop);

            if(
                (
                    !empty($order->getCustomStatus())
                    && !empty($order->getCustomStatus()['permalink'])
                    && $order->getCustomStatus()['permalink'] === $configuration->getCanceledStatus()
                )
                || $order->getStatus() === $configuration->getCanceledStatus()
            )
            {

                $dispatcher->dispatch(
                    Event\OrderCancel::NAME,
                    (
                        (new Event\OrderCancel())
                            ->setShop($shop)
                            ->setOrder($order)
                            ->setConfiguration($configuration)
                    )
                );
            }

            if($order->isUpdateDeal())
            {
                if($order->isPayed())
                {
                    $dispatcher->dispatch(
                        Event\OrderCancel::NAME,
                        (
                            (new Event\OrderCancel())
                                ->setShop($shop)
                                ->setOrder($order)
                                ->setConfiguration($configuration)
                        )
                    );
                }
                else
                {
                    $dispatcher->dispatch(
                        Event\OrderUpdateDeal::NAME,
                        (
                            (new Event\OrderUpdateDeal())
                                ->setShop($shop)
                                ->setConfiguration($configuration)
                                ->setOrder($order)
                        )
                    );
                }
            }

            return $response;
        }

        /**
         * @param Request $request
         * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
         * @param \App\Services\Deal $dealService
         * @param \App\Services\DealEvent $dealEvent
         *
         * @param LoggerInterface $logger
         * @return Response
         * @throws \Exception
         */
        public function nalozhka(
            Request $request,
            EventDispatcherInterface $dispatcher,
            Services\Deal $dealService,
            Services\DealEvent $dealEvent,
            LoggerInterface $logger
        )
        {

            /** @var array $fields */
            $fields = \json_decode($request->getContent(), true);

            /** @var Entity\Deal|null $deal */
            $deal = $dealService->getByDealId($fields['deal']['~id']);

            if(!$deal instanceof Entity\Deal)
            {

                $logger->error('error find deal', $fields);
                return new Response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            /** @var Entity\Configuration $configuration */
            $configuration = $this->getConfigurationByShop($deal->getShop());

            /** @var int $result */
            $result = \openssl_verify(
                $request->getContent(),
                \base64_decode($request->query->get('signature')),
                \openssl_get_publickey(
                    'file://'
                    . (
                        $configuration->isTestMode()
                            ? $this->getParameter('keys')['nalozhka']['test']
                            : $this->getParameter('keys')['nalozhka']['prod']
                    )
                ),
                \OPENSSL_ALGO_SHA256
            );

            if (1 !== $result)
            {

                $logger->error('error check sign', $fields);
                return new Response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            switch($fields['type'])
            {

                case Entity\DealEvent::HOOK_TYPE_DEAL_CREATE:
                    $event = $dealEvent->changeDeal($deal, $fields);
                break;

                case Entity\DealEvent::HOOK_TYPE_CHANGE_STATUS:
                    $event = $dealEvent->changeDealStatus($deal, $fields);
                break;
            }

            if(!empty($event) && $event instanceof Entity\DealEvent)
            {

                /** @var Event\DealChangeStatus $eventStatus */
                $eventStatus = new Event\DealChangeStatus();
                $eventStatus->setDealEvent($event);

                $dispatcher->dispatch(Event\DealChangeStatus::NAME, $eventStatus);

                if($event->getNewStatus() === $this->getParameter('app.nalozhka.status.payed'))
                {
                    $dispatcher->dispatch(
                        Event\OrderPayed::NAME,
                        (
                            (new Event\OrderPayed())
                                ->setShop($deal->getShop())
                                ->setDeal($deal)
                                ->setOrder($deal->getOrder())
                            )
                    );
                }
            }

            return new Response(null, Response::HTTP_OK);
        }
    }