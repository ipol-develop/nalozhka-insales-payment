<?php

    namespace App\Controller;

    use App\Services\Deal;
    use App\Services\Insales\Orders;
    use App\Services\Nalozhka\Client;
    use Psr\Log\LoggerInterface;
    use Symfony\Component\HttpFoundation\Request;
    use App\Entity;
    use Symfony\Component\HttpFoundation\Response;

    /**
     * Class Payment
     * @package App\Controller
     */
    class Payment extends Base
    {

        /**
         * Переход на страницу оплаты
         * @param Request $request
         * @param Orders $orders
         * @param Client $nalozhka
         * @param LoggerInterface $logger
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function payOrder(Request $request, Orders $orders, Client $nalozhka, LoggerInterface $logger)
        {

            /** @var Entity\Shop $shop */
            $shop = $this->getShopById($request->request->getInt('shop_id'));

            /** @var Entity\Configuration $configuration */
            $configuration = $this->getConfigurationByShop($shop);

            /** @var Entity\Order $order */
            $order = $orders->getOrderByInsaslesId($shop, $request->request->getInt('order_id'));

            $nalozhka
                ->setShop($shop)
                ->setConfiguration($configuration);

            try
            {

                /** @var Entity\Deal $deal */
                $deal = $nalozhka->createDeal($shop, $order, $configuration);

                if(!$deal instanceof Entity\Deal)
                {

                    $response = $nalozhka->getLastResponse();

                    $logger->error(
                        'deal no found',
                        [
                            'data' => $response->getData(),
                            'headers' => $response->getHeaders(),
                            'http_code' => $response->getHttpCode()
                        ]
                    );

                    return $this->render('payment/start-error.html.twig');
                }

                /** @var array $payment */
                $payment = $nalozhka->getPaymentUrl($deal, $order);
                return $this->redirect($payment['url'] . '?' . \http_build_query($payment['fields']));
            }
            catch (\Exception $e)
            {

                $logger->error(
                    'error create deal',
                    [
                        'orderId' => $order->getOrderNumber(),
                        'message' => $e->getMessage(),
                        'line' => $e->getLine(),
                        'file' => $e->getFile(),
                    ]
                );

                return $this->render('payment/start-error.html.twig');
            }
        }

        /**
         * Получение параметров для отправки в Insales
         *
         * @param Entity\Deal $deal
         * @param Entity\Order $order
         *
         * @return array
         */
        protected function getResultPaymentFields(Entity\Deal $deal, Entity\Order $order)
        {

            /** @var array $fields */
            $fields = [
                'paid' => $order->isPayed() ? 1 : 0,
                'amount' => $order->getPriceOrder(),
                'key' => $order->getOrderKey(),
                'transaction_id' => $order->getTransactionIid(),
                'shop_id' => $deal->getShop()->getInsalesId(),
                'payment_gateway_id' => $deal->getShop()->getPaymentId(),
            ];

            $fields['signature'] = \md5(
                \implode(
                    ';',
                    [
                        $deal->getShop()->getInsalesId(),
                        $fields['amount'],
                        $fields['transaction_id'],
                        $fields['key'],
                        $fields['paid'],
                        $deal->getShop()->getPaymentPassword()
                    ]
                )
            );

            return $fields;
        }

        /**
         * Страница оплаты
         *
         * @param $dealId
         * @param Orders $orders
         * @param \App\Services\Deal $dealService
         *
         * @return Response
         */
        public function paymentRedirect($type, $dealId, Orders $orders, Deal $dealService)
        {

            /** @var Response $response */
            $response = new Response(null, Response::HTTP_INTERNAL_SERVER_ERROR);

            /** @var Entity\Deal $deal */
            $deal = $dealService->getByDealId($dealId);

            if(!$deal instanceof Entity\Deal)
                return $response;

            /** @var Entity\Order $order */
            $order = $orders->downloadOrder($deal->getShop(), $deal->getOrder()->getOrderId());

            if(!$order instanceof Entity\Order)
                return $response;

            return $this->render(
                $type === 'success'
                    ? 'payment/success-page.html.twig'
                    : 'payment/fail-page.html.twig'
                ,
                [
                    'deal' => $deal,
                    'fields' => $this->getResultPaymentFields($deal, $order),
                ]
            );
        }
    }