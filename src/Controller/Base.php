<?php

    namespace App\Controller;

    use App\Entity;

    /**
     * Class AbstractController
     *
     * @package App\Controller
     */
    class Base extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
    {

        /**
         * Получить магазин по insalesId
         *
         * @param $id
         *
         * @return \App\Entity\Shop|object|null
         */
        public function getShopById($id = null)
        {

            $id = $id ?? (int) $this->get('session')->get('insalesId');

            if($id === 0)
                return null;

            return $this->getDoctrine()
                ->getRepository(Entity\Shop::class)
                ->getByInsalesId($id);
        }

        /**
         * Получение настроек по магазину
         * @param Entity\Shop $shop
         * @return Entity\Configuration|object|null
         */
        public function getConfigurationByShop(Entity\Shop $shop)
        {
            return $this->getDoctrine()
                ->getRepository(Entity\Configuration::class)
                ->getByShop($shop);
        }
    }