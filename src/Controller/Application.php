<?php

    namespace App\Controller;

    use App\Services\Logs;
    use Symfony\Component\HttpFoundation\Request;
    use App\Entity;
    use App\Services;
    use Symfony\Contracts\Translation\TranslatorInterface;

    /**
     * Class Application
     *
     * @package App\Controller
     */
    class Application extends Base
    {

        /**
         * Настройка приложения
         *
         * @param \Symfony\Component\HttpFoundation\Request $request
         * @param Services\Insales\Client $client
         * @param \App\Services\Configuration $configure
         *
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function configure(
            Request $request,
            Services\Insales\Client $client,
            Services\Configuration $configure)
        {

            /** @var Entity\Shop $shop */
            $shop = $this->getShopById();

            if(!$shop instanceof Entity\Shop)
                return $this->redirectToRoute('session.end');

            /** @var Entity\Configuration $configuration */
            $configuration = $this->getConfigurationByShop($shop);

            $client->setShop($shop)->setConfiguration($configuration);

            $form = $this->createForm(
                \App\Forms\Configuration::class,
                $configuration,
                [
                    'canceledStatus' => $client->getStatusesCancel(),
                ]
            );

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid())
            {

                $configure->setConfigurationByShop($shop, $form->getData());
                return $this->redirectToRoute('app.configure');
            }

            return $this->render(
                'app/configuration.html.twig',
                [
                    'form' => $form->createView(),

                ]
            );
        }

        /**
         * @param Request $request
         * @param Logs $logs
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function logs(Request $request, Logs $logs)
        {

            /** @var Entity\Shop $shop */
            $shop = $this->getShopById();

            if(!$shop instanceof Entity\Shop)
                return $this->redirectToRoute('session.end');

            /** @var int $page */
            $page = $request->query->getInt('page') === 0 ? 1 : $request->query->getInt('page');

            return $this->render(
                'app/logs.html.twig',
                [
                    'logs' => $logs->getLogsByShop($shop, $page)
                ]
            );
        }

        /**
         * Меню личного кабинета настроек
         *
         * @param \Symfony\Contracts\Translation\TranslatorInterface $translator
         * @param string $route
         *
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function menuWidget(TranslatorInterface $translator, ?string $route)
        {

            /** @var array $menu */
            $menu = [
                ['code' => 'app.configure', 'title' => $translator->trans('app.menu.configuration')],
                ['code' => 'app.logs', 'title' => $translator->trans('app.menu.logs')],
                ['code' => 'app.statuses', 'title' => $translator->trans('app.menu.statuses')],
            ];

            foreach($menu as &$item)
            {
                $item = \array_merge(
                    $item,
                    [
                        'url' => $this->generateUrl($item['code']),
                        'active' => $item['code'] === $route
                    ]
                );
            }

            /** @var Entity\Shop $shop */
            $shop = $this->getShopById();

            return $this->render(
                'app/menu.html.twig',
                [
                    'items' => $menu,
                    'shop' => $shop,
                ]
            );
        }

        /**
         * Связь статусов
         *
         * @param Request $request
         * @param Services\Insales\Client $insales
         * @param Services\Nalozhka\Client $nalozhka
         * @param \App\Services\Statuses $statuses
         *
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function statuses(
            Request $request,
            Services\Insales\Client $insales,
            Services\Nalozhka\Client $nalozhka,
            Services\Statuses $statuses
        )
        {

            /** @var Entity\Shop $shop */
            $shop = $this->getShopById();

            if(!$shop instanceof Entity\Shop)
                return $this->redirectToRoute('session.end');

            /** @var Entity\Configuration $configuration */
            $configuration = $this->getConfigurationByShop($shop);

            $insales->setShop($shop)->setConfiguration($configuration);
            $nalozhka->setShop($shop)->setConfiguration($configuration);

            if($request->getMethod() === Request::METHOD_POST)
            {
                $statuses->setByShop($shop, $request->request->get('statuses') ?? []);
                return $this->redirectToRoute('app.statuses');
            }

            Services\Nalozhka\Client::$noLog = true;

            /** @var array $statusesView */
            $statusesView = $statuses->getByShop($shop, $insales->getStatuses(), $nalozhka->getStatuses());

            Services\Nalozhka\Client::$noLog = false;

            return $this->render(
                'app/statuses.html.twig',
                [
                    'statuses' => $statusesView
                ]
            );
        }

        /**
         * Виджет заказа в админке insales
         *
         * @param \Symfony\Component\HttpFoundation\Request $request
         * @param int $insalesId
         * @param string $widgetPassword
         * @param int $orderId
         * @param \App\Services\Insales\Orders $orders
         * @param \App\Services\Nalozhka\Client $client
         * @param \App\Services\TrackNumber $trackNumber
         * @param \App\Services\Deal $dealService
         * @param \App\Services\NalozhkaStatus $nalozhkaStatus
         *
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function orderWidget(
            Request $request,
            int $insalesId,
            string $widgetPassword,
            int $orderId,
            Services\Insales\Orders $orders,
            Services\Nalozhka\Client $client,
            Services\TrackNumber $trackNumber,
            Services\Deal $dealService,
            Services\NalozhkaStatus $nalozhkaStatus
        )
        {

            /** @var array $errors */
            $errors = [];

            /** @var Entity\Shop $shop */
            $shop = $this->getShopById($insalesId);

            $client->setShop($shop)->setConfiguration($this->getConfigurationByShop($shop));

            if(
                !$shop instanceof Entity\Shop
                || $shop->getWidgetPassword() !== $widgetPassword
            )
                return $this->render('app/order-widget-error.html.twig');

            /** @var Entity\Order $order */
            $order = $orders->downloadOrder($shop, $orderId);

            if(!$order instanceof Entity\Order)
                return $this->render('app/order-widget-error.html.twig');

            if($order->getPaymentId() !== $shop->getPaymentId())
            {
                return $this->render(
                    'app/order-widget-other.html.twig',
                    [
                        'order' => $order,
                    ]
                );
            }

            /** @var Entity\Deal $deal */
            $deal = $dealService->getByShopOrder($order);

            if(
                $request->getMethod() === Request::METHOD_GET
                && $this->isCsrfTokenValid('remove', $request->query->get('token'))
                && $request->query->get('action') === 'removeTrack'
            )
            {

                /** @var Entity\TrackNumber $number */
                $number = $trackNumber->getByShopOrderTrackId($shop, $order, $request->query->getInt('id'));

                if($number instanceof Entity\TrackNumber)
                {

                    /** @var \App\Services\Nalozhka\ApiResponse $response */
                    $response = $client->removeTrackNumber($deal, $number);

                    if($response->isSuccessful())
                    {

                        $trackNumber->removeByTrackingNumber($number);

                        return $this->redirectToRoute(
                            'app.order.widget',
                            [
                                'insalesId' => $shop->getInsalesId(),
                                'widgetPassword' => $shop->getWidgetPassword(),
                                'orderId' => $order->getOrderId(),
                            ]
                        );
                    }

                    $errors[] = $response->getData()['message'];
                }
            }

            if(
                $request->getMethod() === Request::METHOD_POST
                && $this->isCsrfTokenValid('csrf_token', $request->request->get('token'))
                && \mb_strlen($request->request->get('trackNumber')) > 0
            )
            {

                /** @var Entity\TrackNumber $number */
                $number = $trackNumber->createTracking(
                    $shop,
                    $order,
                    $deal,
                    $request->request->get('provider'),
                    $request->request->get('trackNumber')
                );

                /** @var \App\Services\Nalozhka\ApiResponse $response */
                $response = $client->createTrackNumber($deal, $number);

                if($response->isSuccessful())
                {

                    $trackNumber->createByTrackingNumber($number);

                    return $this->redirectToRoute(
                        'app.order.widget',
                        [
                            'insalesId' => $shop->getInsalesId(),
                            'widgetPassword' => $shop->getWidgetPassword(),
                            'orderId' => $order->getOrderId(),
                        ]
                    );
                }

                $errors[] = $response->getData()['message'];
            }

            $status = null;

            if($deal instanceof Entity\Deal)
            {
                /** @var Entity\NalozhkaStatus $status */
                $status = $nalozhkaStatus->getByDeal($deal);
            }

            /** @var Entity\TrackNumber[] $trackings */
            $trackings = $trackNumber->getByShopOrder($shop, $order);

            return $this->render(
                'app/order-widget.html.twig',
                [
                    'status' => $status,
                    'deal' => $deal,
                    'order' => $order,
                    'errors' => $errors,
                    'trackings' => $trackings,
                    'deliveryProviders' => $client->getTrackProviders(),
                ]
            );
        }
    }