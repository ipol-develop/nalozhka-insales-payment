<?php

    namespace App\Forms;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    /**
     * Class Configuration
     * @package App\Forms
     */
    class Configuration extends AbstractType
    {

        /**
         * @param FormBuilderInterface $builder
         * @param array $options
         */
        public function buildForm(FormBuilderInterface $builder, array $options)
        {

            $builder
                ->add(
                    'testMode',
                    Type\CheckboxType::class,
                    [
                        'required' => false,
                        'label' => 'app.configure.testMode',
                    ]
                )
                ->add(
                    'apiKey',
                    Type\TextType::class,
                    [
                        'required' => false,
                        'label' => 'app.configure.apiKey',
                    ]
                )
                ->add(
                    'apiKeyTest',
                    Type\TextType::class,
                    [
                        'required' => false,
                        'label' => 'app.configure.apiKeyTest',
                    ]
                )
                ->add(
                    'devEnv',
                    Type\CheckboxType::class,
                    [
                        'required' => false,
                        'label' => 'app.configure.devEnv',
                    ]
                )
                ->add(
                    'updateStatuses',
                    Type\CheckboxType::class,
                    [
                        'required' => false,
                        'label' => 'app.configure.updateStatuses',
                    ]
                )
                ->add(
                    'useCustomStatuses',
                    Type\CheckboxType::class,
                    [
                        'required' => false,
                        'label' => 'app.configure.useCustomStatuses',
                    ]
                )
                ->add(
                    'canceledStatus',
                    Type\ChoiceType::class,
                    [
                        'label' => 'app.configure.canceledStatus',
                        'required' => true,
                        'choices' => $options['canceledStatus'],
                    ]
                )
            ;
        }

        /**
         * @param OptionsResolver $resolver
         */
        public function configureOptions(OptionsResolver $resolver)
        {

            $resolver->setDefaults(
                [
                    'data_class' => \App\Entity\Configuration::class,
                    'canceledStatus' => null,
                ]
            );
        }
    }