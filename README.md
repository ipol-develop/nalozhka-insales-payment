# Модуль интеграции Наложки в InSales

Модуль выполнен в виде отдельного веб-приложения, написанного на PHP с
использованием фреймворка Symfony.

Это приложение выступает связующим звеном между Наложкой и InSales, пользуется
API одной и другой стороны, а также слушает события (через вебхуки) от той и
от другой стороны.

## Разворачивание приложения

Необходимо установить переменные окружения, указанные в файле `.env.dist`. 
Это можно сделать как непосредственно через работу с окружением в OC, так и
скопировав файл `.env.dist` в `.env.local` и установив в нем корректные 
значения.

После этого необходимо запустить установку внешних зависимостей командой:

```bash
composer install
```
### Настройка задач, выполняемых по расписанию

Для запуска асинхронных процедур необходимо через планировщик задач с интервалом
в минуту запускать следующие консольные команды:

|         Наименование           |                   Описание                  | Файл запуска в директории http-exec |
|--------------------------------|---------------------------------------------|-------------------------------------|
|`app:nalozhka:deal:photo-upload`|загрузка и прикрепления изображений товаров Заказов к Сделкам Наложки|`insales-statuses-update`|
|`app:insales:statuses-update`   |обновление статусов Заказов InSales в соответствии со статусами Сделок Наложки|`nalozhka-deal-photo-upload`|

Если сервис запускается в нескольких репликах в docker-swarm, то для запуска 
процедур по расписанию мы используем внешний планировщик, запускающий скрипты 
через (http-exec)[https://bitbucket.org/nalogka/http-exec].

## Кэширование данных

На данный момент в модуле кэшируется список возможных статусов Сделок Наложки
(коды и человекопонятные названия).

Этот кэш сохраняется навсегда и при изменении списка статусов Сделки в Наложке
должен быть сброшен консольной командой:

```bash
bin/console cache:pool:delete cache.app ipol.insales.nalogka-statuses
```

## Непрерывная интеграция

Сервер непрерывной интеграции в процессе сборки производит как специфичные
для сервиса действия (запуск локальных утилит), так и общие шаги (типа сборка
Docker-образа).

Для описания специфичных шагов сборки добавляем в репозиторий файлы,
обрабатываемые утилитой [myke](https://goeuro.github.io/myke/).

В корне проекта лежит `myke.yml`.

У нас существует образ среды сборки (содержащей необходимые инструменты)
`docker.nalogka.com/builder:php`.

Пример запуска сборки:

```sh
docker run --rm \
        -v shared_composer:/.composer \
        -v $PWD:/project \
    docker.nalogka.com/builder:php myke build
```

где `shared_composer` - docker volume для кэширования пакетов composer-а.

Прогон статического анализатора кода:

```sh
docker run --rm \
        -v shared_composer:/.composer \
        -v $PWD:/project \
    docker.nalogka.com/builder:php myke analyze
```


## Команды

Обновляет статусы в insales, обработка 50 сделок за раз, на крон ставить каждые 5 минут
```bash
bin/console app:insales:statuses-update
```

Выгружает фотографии заказанных товаров с серверов insales, 3 заказа за раз, на крон ставить каждые 5 минут
```bash
bin/console app:nalozhka:deal:photo-upload
```

У insales есть лимит запросов - 500 запросов за 5 минут но не больше 1-го запроса в секунду.  
Если начнутся сбои по обмену информацией - можно уменьшить количество одновременно обрабатываемых заказов / статусов.
(см. параметры ниже)
##Установка приложения

Insales отправляет запрос на установку приложения, ответом должен быть http 200

```yaml
app.install:
    path: /app/install
    controller: App\Controller\Insales::installApplication
```
Если ответом был http 500 есть несколько вариантов:
* Не установились web hook для создания / обновления заказа `error set orders/update(create) webhook`
* Не удалось создать платежную систему `error create pay system`

##Удаление приложентя

Insales отправляет запрос на удаление приложения, ответом должен быть http 200
```yaml
app.remove:
    path: /app/remove
    controller: App\Controller\Insales::removeApplication
```
Если ответом был http 500 есть несколько вариантов:
* Не найден магазин в базе `error remove application, shop not found`


##Создание сделки и пересылка на оплату

Insales отправляет запрос на удаление приложения, ответом должен быть http 200
```yaml
payment.url.pay:
    path: /payment/pay
    controller: App\Controller\Payment::payOrder
```
Если ответом был http 500 есть несколько вариантов:
* Не найдена сделка `deal no found`
* Не удалось создать сделку `error create deal`

##Параметры

Параметры прописаны в файле `config/services.yaml`

| Параметр | Описание | Пример |
| -------- | -------- |:------:|
| nalozhka.file.upload.limit | Количество обрабатываемых за раз заказов для выгрузки фотографий | 3 |
| insales.statuses.update.limit | Количество обрабатываемых за раз заказов сделок для обновления статусов в insales | 50 |
| app.id | ID приложения insales (из insales приложения) | nalozhka_insales_payment |
| app.secret | Код приложения insales app.secret (из insales приложения) | 4E8ED165D86E5CEF561623723AD98D15 |
| app.http | Адрес сервера с приложением (http)  (на данный url приходят запросы от insales, а так же это URL индексной страницы данного приложения) | http://service.cloud.net |
| app.nalozhka.status.payed | Статус оплаты у наложки | waiting_performing |
| app.insales.payed.payed | Статус оплаты заказа у insales | paid |
| app.insales.payed.no-payed | Статус не оплаченного заказа у insales | pending |
| no_auth_url | Роуты по которым можно делать запрос без авторизации (установка, удаление, переход к оплате, хуки) |  - 'app.configure' |

Пути к файлам ключей находяться в структуре keys / nalozhka

```yaml
keys:
    nalozhka:
        prod: 'nalogka-rsa-public.pem'
        test: 'nalogka-sandbox-rsa-public.pem'
```
`keys / nalozhka / prod` Путь к боевому файлу ключей  
`keys / nalozhka / test` Путь к тестовому файлу ключей